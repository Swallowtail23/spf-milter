# SPF Milter changelog

## 0.3.1 (unreleased)

*   Insert trace headers before the MTA’s *Received* header instead of after it.
*   Convey both HELO and MAIL FROM verification results in a single
    *Authentication-Results* header.

## 0.3.0 (2021-05-20)

*   (defaults and behaviour change) The default value of the
    `definitive_helo_results` parameter is now the empty set (instead of `pass,
    fail`). At the same time, the parameter `reject_helo_results` now applies
    unconditionally and is no longer constrained by the value of
    `definitive_helo_results`.

    With this change, use of `definitive_helo_results` has been deemphasised in
    the documentation. While this parameter is necessary for full RFC
    conformance, we find it is more of a niche feature in practice, and not
    appropriate to enable by default.
*   Add parameter `skip_senders_file` to allow specifying a list of sender
    identities to ignore (ie, senders bypass SPF verification).
*   Add parameter `include_all_results` to allow adding all available
    verification results (HELO and MAIL FROM) to the message header, instead of
    just one of them.
*   Update dependencies.

## 0.2.2 (2021-03-27)

*   Update dependencies.

## 0.2.1 (2021-03-01)

*   Add parameter `include_mailfrom_local_part` to allow reporting the
    *local-part* of mailboxes in *Authentication-Results* headers.
*   Properly specify minimal dependency versions in `Cargo.toml`.
*   Update dependencies.

## 0.2.0 (2021-01-12)

*   (behaviour change) The HELO identity is now verified at the `helo` stage of
    the milter protocol instead of at the `mail` stage
    ([glts/spf-milter#1](https://gitlab.com/glts/spf-milter/-/issues/1)).

    While this is mainly an implementation change, it is nevertheless observable
    in certain cases. In particular, the sender authentication status is not
    available at the `helo` stage and cannot be taken into account for the HELO
    authorisation decision; users should revisit and if necessary adjust their
    setup (eg, disable SPF Milter on the submission port).
*   Add support for writing debug log output to syslog in integration tests
    (environment variable `SPF_MILTER_TEST_LOG`).
*   Update dependency `signal-hook` to version 0.3.
*   Update dependencies in `Cargo.lock` file.

## 0.1.0 (2020-11-28)

Initial release.
