use clap::{App, Arg, ArgMatches, Error, ErrorKind};
use spf_milter::CliOptions;
use std::process;

const ARG_CONFIG_FILE: &str = "CONFIG_FILE";
const ARG_DRY_RUN: &str = "DRY_RUN";
const ARG_LOG_DESTINATION: &str = "LOG_DESTINATION";
const ARG_LOG_LEVEL: &str = "LOG_LEVEL";
const ARG_SOCKET: &str = "SOCKET";
const ARG_SYSLOG_FACILITY: &str = "SYSLOG_FACILITY";

fn main() {
    let matches = App::new(spf_milter::MILTER_NAME)
        .version(spf_milter::VERSION)
        .arg(Arg::with_name(ARG_CONFIG_FILE)
            .short("c")
            .long("config-file")
            .value_name("PATH")
            .help("Path to configuration file"))
        .arg(Arg::with_name(ARG_DRY_RUN)
            .short("n")
            .long("dry-run")
            .help("Process messages without taking action"))
        .arg(Arg::with_name(ARG_LOG_DESTINATION)
            .short("l")
            .long("log-destination")
            .value_name("TARGET")
            .help("Destination for log messages"))
        .arg(Arg::with_name(ARG_LOG_LEVEL)
            .short("L")
            .long("log-level")
            .value_name("LEVEL")
            .help("Minimum severity of messages to log"))
        .arg(Arg::with_name(ARG_SOCKET)
            .short("p")
            .long("socket")
            .value_name("SOCKET")
            .help("Listening socket of the milter"))
        .arg(Arg::with_name(ARG_SYSLOG_FACILITY)
            .short("s")
            .long("syslog-facility")
            .value_name("NAME")
            .help("Facility to use for syslog messages"))
        .get_matches();

    let opts = match build_opts(&matches) {
        Ok(opts) => opts,
        Err(e) => {
            e.exit();
        }
    };

    if let Err(e) = spf_milter::run(opts) {
        eprintln!("error: {}", e);
        process::exit(1);
    }
}

fn build_opts(matches: &ArgMatches<'_>) -> clap::Result<CliOptions> {
    let mut opts = CliOptions::builder();

    if let Some(path) = matches.value_of(ARG_CONFIG_FILE) {
        opts = opts.config_file(path);
    }
    if matches.is_present(ARG_DRY_RUN) {
        opts = opts.dry_run(true);
    }
    if let Some(socket) = matches.value_of(ARG_SOCKET) {
        opts = opts.socket(socket);
    }
    if let Some(target) = matches.value_of(ARG_LOG_DESTINATION) {
        match target.parse() {
            Ok(target) => {
                opts = opts.log_destination(target);
            }
            Err(_) => {
                return Err(Error::with_description(
                    &format!("Invalid value for log destination: \"{}\"", target),
                    ErrorKind::InvalidValue,
                ));
            }
        }
    }
    if let Some(level) = matches.value_of(ARG_LOG_LEVEL) {
        match level.parse() {
            Ok(level) => {
                opts = opts.log_level(level);
            }
            Err(_) => {
                return Err(Error::with_description(
                    &format!("Invalid value for log level: \"{}\"", level),
                    ErrorKind::InvalidValue,
                ));
            }
        }
    }
    if let Some(name) = matches.value_of(ARG_SYSLOG_FACILITY) {
        match name.parse() {
            Ok(name) => {
                opts = opts.syslog_facility(name);
            }
            Err(_) => {
                return Err(Error::with_description(
                    &format!("Invalid value for syslog facility: \"{}\"", name),
                    ErrorKind::InvalidValue,
                ));
            }
        }
    }

    Ok(opts.build())
}
