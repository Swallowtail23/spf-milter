use crate::config::model::{LogDestination, LogLevel, SyslogFacility};
use once_cell::sync::OnceCell;
use std::path::{Path, PathBuf};

static CLI_OPTIONS: OnceCell<CliOptions> = OnceCell::new();

/// Initialises the static CLI options.
///
/// Must be called exactly once during startup.
pub fn init(opts: CliOptions) {
    CLI_OPTIONS.set(opts).expect("CLI options already initialized");
}

/// Returns a reference to the static CLI options.
pub fn get() -> &'static CliOptions {
    CLI_OPTIONS.get().expect("CLI options not initialized")
}

// Allow overriding the default configuration file path during the build.
const DEFAULT_CONFIG_FILE: &str = match option_env!("SPF_MILTER_CONFIG_FILE") {
    Some(s) => s,
    None => "/etc/spf-milter.conf",
};

/// A set of CLI options.
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct CliOptions {
    // The configuration file path defaults to `/etc/spf-milter.conf` and is
    // always present, even if not set on the command-line.
    config_file: PathBuf,
    dry_run: bool,
    log_destination: Option<LogDestination>,
    log_level: Option<LogLevel>,
    socket: Option<String>,
    syslog_facility: Option<SyslogFacility>,
}

impl CliOptions {
    pub fn builder() -> CliOptionsBuilder {
        CliOptionsBuilder::new()
    }

    pub fn config_file(&self) -> &Path {
        &self.config_file
    }

    pub fn dry_run(&self) -> bool {
        self.dry_run
    }

    pub fn log_destination(&self) -> Option<LogDestination> {
        self.log_destination
    }

    pub fn log_level(&self) -> Option<LogLevel> {
        self.log_level
    }

    pub fn socket(&self) -> Option<&str> {
        self.socket.as_deref()
    }

    pub fn syslog_facility(&self) -> Option<SyslogFacility> {
        self.syslog_facility
    }
}

/// A builder for CLI options.
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct CliOptionsBuilder {
    config_file: Option<PathBuf>,
    dry_run: bool,
    log_destination: Option<LogDestination>,
    log_level: Option<LogLevel>,
    socket: Option<String>,
    syslog_facility: Option<SyslogFacility>,
}

impl CliOptionsBuilder {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn config_file<P: Into<PathBuf>>(mut self, value: P) -> Self {
        self.config_file = Some(value.into());
        self
    }

    pub fn dry_run(mut self, value: bool) -> Self {
        self.dry_run = value;
        self
    }

    pub fn log_destination(mut self, value: LogDestination) -> Self {
        self.log_destination = Some(value);
        self
    }

    pub fn log_level(mut self, value: LogLevel) -> Self {
        self.log_level = Some(value);
        self
    }

    pub fn socket<S: Into<String>>(mut self, value: S) -> Self {
        self.socket = Some(value.into());
        self
    }

    pub fn syslog_facility(mut self, value: SyslogFacility) -> Self {
        self.syslog_facility = Some(value);
        self
    }

    pub fn build(self) -> CliOptions {
        CliOptions {
            config_file: self
                .config_file
                .unwrap_or_else(|| PathBuf::from(DEFAULT_CONFIG_FILE)),
            dry_run: self.dry_run,
            log_destination: self.log_destination,
            log_level: self.log_level,
            socket: self.socket,
            syslog_facility: self.syslog_facility,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn cli_options_builder_ok() {
        let opts = CliOptions::builder().socket("inet:3000@localhost").build();

        assert_eq!(opts.socket(), Some("inet:3000@localhost"));
        assert_eq!(opts.config_file(), Path::new("/etc/spf-milter.conf"));
    }
}
