use ipnet::IpNet;
use log::LevelFilter;
use std::{
    collections::HashSet,
    error::Error,
    fmt::{self, Display, Formatter},
    net::IpAddr,
    str::FromStr,
};
use syslog::Facility;
use viaspf::{record::ExplainString, Name, SpfResult};

#[derive(Clone, Copy, Debug, Default, Eq, Hash, PartialEq)]
pub struct ParseStatusCodeError;

impl Error for ParseStatusCodeError {}

impl Display for ParseStatusCodeError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "failed to parse status code")
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum ReplyCode {
    Transient(String),
    Permanent(String),
}

impl AsRef<str> for ReplyCode {
    fn as_ref(&self) -> &str {
        match self {
            Self::Transient(s) | Self::Permanent(s) => s,
        }
    }
}

impl Display for ReplyCode {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.as_ref().fmt(f)
    }
}

impl FromStr for ReplyCode {
    type Err = ParseStatusCodeError;

    // See RFC 5321, section 4.2.
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.as_bytes() {
            [x, y, z]
                if matches!(x, b'4'..=b'5')
                    && matches!(y, b'0'..=b'5')
                    && matches!(z, b'0'..=b'9') =>
            {
                Ok(match x {
                    b'4' => Self::Transient(s.into()),
                    b'5' => Self::Permanent(s.into()),
                    _ => unreachable!(),
                })
            }
            _ => Err(ParseStatusCodeError),
        }
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum EnhancedStatusCode {
    Transient(String),
    Permanent(String),
}

impl EnhancedStatusCode {
    pub fn is_compatible_with(&self, reply_code: &ReplyCode) -> bool {
        matches!(
            (self, reply_code),
            (Self::Transient(_), ReplyCode::Transient(_))
                | (Self::Permanent(_), ReplyCode::Permanent(_))
        )
    }
}

impl AsRef<str> for EnhancedStatusCode {
    fn as_ref(&self) -> &str {
        match self {
            Self::Transient(s) | Self::Permanent(s) => s,
        }
    }
}

impl Display for EnhancedStatusCode {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.as_ref().fmt(f)
    }
}

impl FromStr for EnhancedStatusCode {
    type Err = ParseStatusCodeError;

    // See RFC 3463, section 2.
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        fn is_three_digits(s: &str) -> bool {
            s == "0"
                || matches!(s.len(), 1..=3)
                    && s.chars().all(|c| c.is_ascii_digit())
                    && !s.starts_with('0')
        }

        let mut iter = s.splitn(3, '.');
        match (iter.next(), iter.next(), iter.next()) {
            (Some(class), Some(subject), Some(detail))
                if matches!(class, "4" | "5")
                    && is_three_digits(subject)
                    && is_three_digits(detail) =>
            {
                Ok(match class {
                    "4" => Self::Transient(s.into()),
                    "5" => Self::Permanent(s.into()),
                    _ => unreachable!(),
                })
            }
            _ => Err(ParseStatusCodeError),
        }
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Header(Vec<HeaderType>);

impl Header {
    pub fn new(header_types: Vec<HeaderType>) -> Result<Self, Vec<HeaderType>> {
        // Each header type must not appear more than once (ordered set).
        if header_types.len() == header_types.iter().collect::<HashSet<_>>().len() {
            Ok(Self(header_types))
        } else {
            Err(header_types)
        }
    }

    pub fn iter(&self) -> impl DoubleEndedIterator<Item = &HeaderType> {
        self.0.iter()
    }
}

impl Default for Header {
    fn default() -> Self {
        HeaderType::ReceivedSpf.into()
    }
}

impl From<Vec<HeaderType>> for Header {
    fn from(header_types: Vec<HeaderType>) -> Self {
        Self(header_types)
    }
}

impl From<HeaderType> for Header {
    fn from(header_type: HeaderType) -> Self {
        vec![header_type].into()
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, Hash, PartialEq)]
pub struct ParseHeaderTypeError;

impl Error for ParseHeaderTypeError {}

impl Display for ParseHeaderTypeError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "failed to parse header type")
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum HeaderType {
    ReceivedSpf,
    AuthenticationResults,
}

impl Display for HeaderType {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::ReceivedSpf => write!(f, "Received-SPF"),
            Self::AuthenticationResults => write!(f, "Authentication-Results"),
        }
    }
}

impl FromStr for HeaderType {
    type Err = ParseHeaderTypeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // Ignore case, simply because of conventions around header names.
        if s.eq_ignore_ascii_case("Received-SPF") {
            Ok(HeaderType::ReceivedSpf)
        } else if s.eq_ignore_ascii_case("Authentication-Results") {
            Ok(HeaderType::AuthenticationResults)
        } else {
            Err(ParseHeaderTypeError)
        }
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, Hash, PartialEq)]
pub struct ParseResultKindError;

impl Error for ParseResultKindError {}

impl Display for ParseResultKindError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "failed to parse SPF result kind")
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum RejectResultKind {
    Fail,
    Softfail,
    Temperror,
    Permerror,
}

impl RejectResultKind {
    fn from_spf_result(spf_result: &SpfResult) -> Option<Self> {
        use SpfResult::*;
        match spf_result {
            None | Neutral | Pass => Option::None,
            Fail(_) => Some(Self::Fail),
            Softfail => Some(Self::Softfail),
            Temperror => Some(Self::Temperror),
            Permerror => Some(Self::Permerror),
        }
    }
}

impl FromStr for RejectResultKind {
    type Err = ParseResultKindError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "fail" => Ok(Self::Fail),
            "softfail" => Ok(Self::Softfail),
            "temperror" => Ok(Self::Temperror),
            "permerror" => Ok(Self::Permerror),
            _ => Err(ParseResultKindError),
        }
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct RejectResults(HashSet<RejectResultKind>);

impl RejectResults {
    pub fn includes(&self, result: &SpfResult) -> bool {
        matches!(RejectResultKind::from_spf_result(result), Some(k) if self.0.contains(&k))
    }
}

impl Default for RejectResults {
    fn default() -> Self {
        let mut results = HashSet::new();
        results.insert(RejectResultKind::Fail);
        results.insert(RejectResultKind::Temperror);
        results.insert(RejectResultKind::Permerror);
        results.into()
    }
}

impl From<HashSet<RejectResultKind>> for RejectResults {
    fn from(results: HashSet<RejectResultKind>) -> Self {
        Self(results)
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum DefinitiveHeloResultKind {
    Pass,
    Fail,
    Softfail,
    Temperror,
    Permerror,
}

impl DefinitiveHeloResultKind {
    fn from_spf_result(spf_result: &SpfResult) -> Option<Self> {
        use SpfResult::*;
        match spf_result {
            None | Neutral => Option::None,
            Pass => Some(Self::Pass),
            Fail(_) => Some(Self::Fail),
            Softfail => Some(Self::Softfail),
            Temperror => Some(Self::Temperror),
            Permerror => Some(Self::Permerror),
        }
    }
}

impl FromStr for DefinitiveHeloResultKind {
    type Err = ParseResultKindError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "pass" => Ok(Self::Pass),
            "fail" => Ok(Self::Fail),
            "softfail" => Ok(Self::Softfail),
            "temperror" => Ok(Self::Temperror),
            "permerror" => Ok(Self::Permerror),
            _ => Err(ParseResultKindError),
        }
    }
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct DefinitiveHeloResults(HashSet<DefinitiveHeloResultKind>);

impl DefinitiveHeloResults {
    pub fn includes(&self, result: &SpfResult) -> bool {
        matches!(DefinitiveHeloResultKind::from_spf_result(result), Some(k) if self.0.contains(&k))
    }
}

impl From<HashSet<DefinitiveHeloResultKind>> for DefinitiveHeloResults {
    fn from(results: HashSet<DefinitiveHeloResultKind>) -> Self {
        Self(results)
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum ExpModification {
    Substitute(ExplainString),
    Decorate {
        prefix: ExplainString,
        suffix: ExplainString,
    },
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct TrustedNetworks {
    pub trust_loopback: bool,
    pub networks: HashSet<IpNet>,
}

impl TrustedNetworks {
    pub fn contains(&self, addr: IpAddr) -> bool {
        self.trust_loopback && addr.is_loopback() || self.networks.iter().any(|n| n.contains(&addr))
    }
}

impl Default for TrustedNetworks {
    fn default() -> Self {
        Self {
            trust_loopback: true,
            networks: Default::default(),
        }
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct SkipEntry {
    pub local_part: Option<String>,
    pub domain: Name,
    pub match_subdomains: bool,
}

impl SkipEntry {
    fn matches(&self, sender: &str) -> bool {
        fn matches_domain(entry: &SkipEntry, domain: &Name) -> bool {
            if entry.match_subdomains {
                domain.is_subdomain_of(&entry.domain)
            } else {
                domain == &entry.domain
            }
        }

        fn matches_local_part(entry: &SkipEntry, local_part: Option<&str>) -> bool {
            match (&local_part, &entry.local_part) {
                (Some(lp1), Some(lp2)) => {
                    // Case-insensitive comparison of local-parts, despite case
                    // being theoretically significant according to RFCs.
                    lp1.eq_ignore_ascii_case(lp2)
                }
                (None, Some(_)) => false,
                (_, None) => true,
            }
        }

        let (local_part, domain) = match sender.rfind('@') {
            Some(i) => (Some(&sender[..i]), &sender[i + 1..]),
            None => (None, sender),
        };

        match Name::domain(domain) {
            Ok(name) => matches_domain(self, &name) && matches_local_part(self, local_part),
            Err(_) => false,
        }
    }
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct SkipSenders(HashSet<SkipEntry>);

impl SkipSenders {
    pub fn includes(&self, sender: &str) -> bool {
        self.0.iter().any(|e| e.matches(sender))
    }
}

impl From<HashSet<SkipEntry>> for SkipSenders {
    fn from(results: HashSet<SkipEntry>) -> Self {
        Self(results)
    }
}

/// An error indicating that a log destination could not be parsed.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, PartialEq)]
pub struct ParseLogDestinationError;

impl Error for ParseLogDestinationError {}

impl Display for ParseLogDestinationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "failed to parse log destination")
    }
}

/// The log destination.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum LogDestination {
    Syslog,
    Stderr,
}

impl Default for LogDestination {
    fn default() -> Self {
        Self::Syslog
    }
}

impl FromStr for LogDestination {
    type Err = ParseLogDestinationError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "syslog" => Ok(Self::Syslog),
            "stderr" => Ok(Self::Stderr),
            _ => Err(ParseLogDestinationError),
        }
    }
}

/// An error indicating that a log level could not be parsed.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, PartialEq)]
pub struct ParseLogLevelError;

impl Error for ParseLogLevelError {}

impl Display for ParseLogLevelError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "failed to parse log level")
    }
}

/// The log level.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum LogLevel {
    Error,
    Warn,
    Info,
    Debug,
}

impl Default for LogLevel {
    fn default() -> Self {
        Self::Info
    }
}

impl FromStr for LogLevel {
    type Err = ParseLogLevelError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "error" => Ok(Self::Error),
            "warn" => Ok(Self::Warn),
            "info" => Ok(Self::Info),
            "debug" => Ok(Self::Debug),
            _ => Err(ParseLogLevelError),
        }
    }
}

impl From<LogLevel> for LevelFilter {
    fn from(log_level: LogLevel) -> Self {
        use LogLevel::*;
        match log_level {
            Error => Self::Error,
            Warn => Self::Warn,
            Info => Self::Info,
            Debug => Self::Debug,
        }
    }
}

/// An error indicating that a syslog facility could not be parsed.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, PartialEq)]
pub struct ParseSyslogFacilityError;

impl Error for ParseSyslogFacilityError {}

impl Display for ParseSyslogFacilityError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "failed to parse syslog facility")
    }
}

/// The syslog facility.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum SyslogFacility {
    Auth,
    Authpriv,
    Cron,
    Daemon,
    Ftp,
    Kern,
    Local0,
    Local1,
    Local2,
    Local3,
    Local4,
    Local5,
    Local6,
    Local7,
    Lpr,
    Mail,
    News,
    Syslog,
    User,
    Uucp,
}

impl Default for SyslogFacility {
    fn default() -> Self {
        Self::Mail
    }
}

impl FromStr for SyslogFacility {
    type Err = ParseSyslogFacilityError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "auth" => Ok(Self::Auth),
            "authpriv" => Ok(Self::Authpriv),
            "cron" => Ok(Self::Cron),
            "daemon" => Ok(Self::Daemon),
            "ftp" => Ok(Self::Ftp),
            "kern" => Ok(Self::Kern),
            "local0" => Ok(Self::Local0),
            "local1" => Ok(Self::Local1),
            "local2" => Ok(Self::Local2),
            "local3" => Ok(Self::Local3),
            "local4" => Ok(Self::Local4),
            "local5" => Ok(Self::Local5),
            "local6" => Ok(Self::Local6),
            "local7" => Ok(Self::Local7),
            "lpr" => Ok(Self::Lpr),
            "mail" => Ok(Self::Mail),
            "news" => Ok(Self::News),
            "syslog" => Ok(Self::Syslog),
            "user" => Ok(Self::User),
            "uucp" => Ok(Self::Uucp),
            _ => Err(ParseSyslogFacilityError),
        }
    }
}

impl From<SyslogFacility> for Facility {
    fn from(syslog_facility: SyslogFacility) -> Self {
        use SyslogFacility::*;
        match syslog_facility {
            Auth => Self::LOG_AUTH,
            Authpriv => Self::LOG_AUTHPRIV,
            Cron => Self::LOG_CRON,
            Daemon => Self::LOG_DAEMON,
            Ftp => Self::LOG_FTP,
            Kern => Self::LOG_KERN,
            Local0 => Self::LOG_LOCAL0,
            Local1 => Self::LOG_LOCAL1,
            Local2 => Self::LOG_LOCAL2,
            Local3 => Self::LOG_LOCAL3,
            Local4 => Self::LOG_LOCAL4,
            Local5 => Self::LOG_LOCAL5,
            Local6 => Self::LOG_LOCAL6,
            Local7 => Self::LOG_LOCAL7,
            Lpr => Self::LOG_LPR,
            Mail => Self::LOG_MAIL,
            News => Self::LOG_NEWS,
            Syslog => Self::LOG_SYSLOG,
            User => Self::LOG_USER,
            Uucp => Self::LOG_UUCP,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use ipnet::Ipv4Net;
    use std::net::Ipv6Addr;

    #[test]
    fn reply_code_parse_ok() {
        let code = "441".parse::<ReplyCode>();
        assert_eq!(code, Ok(ReplyCode::Transient("441".into())));
        let code = "499".parse::<ReplyCode>();
        assert_eq!(code, Err(ParseStatusCodeError));
    }

    #[test]
    fn enhanced_status_code_parse_ok() {
        let code = "4.1.23".parse::<EnhancedStatusCode>();
        assert_eq!(code, Ok(EnhancedStatusCode::Transient("4.1.23".into())));
        let code = "4.0.23".parse::<EnhancedStatusCode>();
        assert_eq!(code, Ok(EnhancedStatusCode::Transient("4.0.23".into())));
        let code = "4.01.23".parse::<EnhancedStatusCode>();
        assert_eq!(code, Err(ParseStatusCodeError));
    }

    #[test]
    fn trusted_networks_loopback_ok() {
        let trusted_networks = TrustedNetworks::default();

        assert!(trusted_networks.contains(IpAddr::from([127, 0, 1, 0])));
        assert!(trusted_networks.contains(Ipv6Addr::LOCALHOST.into()));
    }

    #[test]
    fn trusted_networks_subnet_ok() {
        let trusted_networks = TrustedNetworks {
            networks: {
                let mut networks = HashSet::new();
                networks.insert(Ipv4Net::new([43, 5, 0, 0].into(), 16).unwrap().into());
                networks
            },
            ..Default::default()
        };

        assert!(trusted_networks.contains(IpAddr::from([43, 5, 117, 8])));
    }

    #[test]
    fn skip_senders_ok() {
        let skip_senders = SkipSenders::from({
            let mut entries = HashSet::new();
            entries.insert(SkipEntry {
                local_part: None,
                domain: Name::domain("example.com").unwrap(),
                match_subdomains: false,
            });
            entries.insert(SkipEntry {
                local_part: None,
                domain: Name::domain("super.example.com").unwrap(),
                match_subdomains: true,
            });
            entries.insert(SkipEntry {
                local_part: Some("from".into()),
                domain: Name::domain("example.org").unwrap(),
                match_subdomains: false,
            });
            entries
        });

        assert!(skip_senders.includes("Example.Com"));
        assert!(skip_senders.includes("Me@Example.Com"));
        assert!(!skip_senders.includes("super.example.com"));
        assert!(skip_senders.includes("mx1.super.example.com"));
        assert!(!skip_senders.includes("me@super.example.com"));
        assert!(skip_senders.includes("me@mx1.super.example.com"));
        assert!(!skip_senders.includes("example.org"));
        assert!(!skip_senders.includes("mail.example.org"));
        assert!(!skip_senders.includes("to@example.org"));
        assert!(skip_senders.includes("FROM@example.org"));
    }
}
