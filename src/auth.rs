use crate::{
    config::{
        model::{EnhancedStatusCode, HeaderType, ReplyCode},
        Config,
    },
    header::{
        auth_results::{self, AuthenticationResultsHeader},
        received_spf::ReceivedSpfHeader,
        ToHeaderField,
    },
    resolver::MockResolver,
    verify::{self, VerificationResult},
};
use log::{debug, info};
use milter::{ActionContext, SetErrorReply, Status};
use std::{borrow::Cow, net::IpAddr, sync::Arc};
use viaspf::SpfResult;

// A trait for safe SPF result string representations. (We don’t want to use
// `SpfResult`’s `Display` implementation, because the `Fail` variant may
// include the explanation string in brackets.)
pub trait SpfResultKind {
    fn kind(&self) -> &'static str;
}

impl SpfResultKind for SpfResult {
    fn kind(&self) -> &'static str {
        match self {
            Self::None => "none",
            Self::Neutral => "neutral",
            Self::Pass => "pass",
            Self::Fail(_) => "fail",
            Self::Softfail => "softfail",
            Self::Temperror => "temperror",
            Self::Permerror => "permerror",
        }
    }
}

/// Connection-scoped session data.
#[derive(Clone, Debug, Default, Eq, Hash, PartialEq)]
struct ConnectionData {
    hostname: Option<String>,
    ip: Option<IpAddr>,
    helo_host: Option<String>,
    helo_result: Option<VerificationResult>,
}

impl ConnectionData {
    fn new() -> Self {
        Default::default()
    }

    /// Returns the session’s hostname, which is always available after the
    /// `connect` stage.
    fn hostname(&self) -> &str {
        self.hostname.as_deref().expect("no hostname available")
    }

    /// Returns the session’s IP address, which is always available after the
    /// `connect` stage.
    fn ip(&self) -> IpAddr {
        self.ip.expect("no IP address available")
    }
}

/// Message-scoped session data.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
struct MessageData {
    // At the end of the authorisation process `results` may contain the HELO or
    // MAIL FROM result, both, or neither (when senders were skipped).
    results: Vec<VerificationResult>,
    auth_results_i: usize,
    auth_results_deletions: Vec<usize>,
}

impl MessageData {
    fn new(results: Vec<VerificationResult>) -> Self {
        Self {
            results,
            auth_results_i: 0,
            auth_results_deletions: Vec::new(),
        }
    }
}

/// An authorisation session, modeling the sequence of steps that occur as SPF
/// processing is performed on data arriving via the milter callbacks.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct AuthSession {
    /// A reference to this session’s configuration.
    pub config: Arc<Config>,

    conn: ConnectionData,
    message: Option<MessageData>,
}

impl AuthSession {
    pub fn new(config: Arc<Config>) -> Self {
        // Since configuration can be reloaded and change, the session’s
        // configuration is obtained at the very beginning and then remains
        // immutable for the lifetime of the connection.
        Self {
            config,
            conn: ConnectionData::new(),
            message: None,
        }
    }

    /// Initialises this session’s connection information.
    pub fn init_connection(&mut self, hostname: impl Into<String>, ip: impl Into<IpAddr>) {
        self.conn.hostname = Some(hostname.into());
        self.conn.ip = Some(ip.into());
    }

    /// Updates this session’s HELO information and optionally performs
    /// authorisation for the HELO identity, rejecting it if necessary.
    ///
    /// May be called more than once per connection, because the `HELO`/`EHLO`
    /// SMTP command may be given multiple times when STARTTLS is used. See RFC
    /// 3207, section 4.2.
    pub fn authorize_helo(
        &mut self,
        context: &(impl ActionContext + SetErrorReply),
        helo_host: impl Into<String>,
        mock_resolver: Option<&MockResolver>,
    ) -> milter::Result<Status> {
        self.conn.helo_host = Some(helo_host.into());

        if self.config.verify_helo() {
            let helo_host = self.conn.helo_host.as_ref().unwrap();

            // Clear a superseded result from an earlier invocation.
            let prev_result = self.conn.helo_result.take();

            if self.config.skip_senders().includes(helo_host) {
                return skip_sender(helo_host);
            }

            // Unlike the MAIL FROM identity, the HELO identity may evaluate to
            // `None` (no result) if it is not a fully-qualified domain name.
            if let Some(result) = verify::verify_helo_identity(
                &self.config,
                mock_resolver,
                self.conn.hostname(),
                self.conn.ip(),
                helo_host,
            ) {
                // As the `helo` stage may be called more than once (and usually
                // is when STARTTLS is used), omit the repeated log line.
                log_verification_result(&result, matches!(prev_result, Some(r) if r == result));

                // If the result is in the set of results to be rejected, do so
                // now, else remember the result for later processing.
                if self.config.reject_helo_results().includes(&result.spf_result) {
                    return reject_sender(context, &self.config, "connection", &result);
                }

                self.conn.helo_result = Some(result);
            } else {
                debug!("not verifying non-FQDN HELO identity \"{}\"", helo_host);
            }
        }

        Ok(Status::Continue)
    }

    /// Performs authorisation for the MAIL FROM identity when requested,
    /// rejecting it if necessary. After this step, the final set of SPF results
    /// is established.
    pub fn authorize_mail_from(
        &mut self,
        context: &(impl ActionContext + SetErrorReply),
        mail_from: &str,
        mock_resolver: Option<&MockResolver>,
    ) -> milter::Result<Status> {
        let mut results = Vec::new();

        if let Some(result) = &self.conn.helo_result {
            // If HELO identity verification was done earlier and a definitive
            // result is available, return early, skipping MAIL FROM
            // verification.
            if self.config.definitive_helo_results().includes(&result.spf_result) {
                results.push(result.clone());

                let message = MessageData::new(results);

                self.message = Some(message);

                return Ok(Status::Continue);
            }

            if self.config.include_all_results() {
                results.push(result.clone());
            }
        }

        let helo_host = self.conn.helo_host.as_deref().unwrap_or("unknown");

        let mail_from = verify::prepare_mail_from_identity(mail_from, helo_host);

        if self.config.skip_senders().includes(&mail_from) {
            let message = MessageData::new(results);

            self.message = Some(message);

            return skip_sender(&mail_from);
        }

        // Unlike the HELO identity, the MAIL FROM identity always evaluates to
        // a (‘definitive’) result. Of course, an unusable MAIL FROM identity
        // simply evaluates to *none* according to the spec.
        let result = verify::verify_mail_from_identity(
            &self.config,
            mock_resolver,
            self.conn.hostname(),
            self.conn.ip(),
            &mail_from,
            helo_host,
        );
        log_verification_result(&result, false);

        if self.config.reject_results().includes(&result.spf_result) {
            return reject_sender(context, &self.config, "message", &result);
        }

        results.push(result);

        let message = MessageData::new(results);

        self.message = Some(message);

        Ok(Status::Continue)
    }

    /// Examines incoming `Authentication-Results` headers to detect forged
    /// input (those using our *authserv-id*).
    pub fn process_auth_results_header(&mut self, id: &str, value: &str) {
        // See RFC 8601, section 5.

        if self.config.delete_incoming_authentication_results() {
            let message = self
                .message
                .as_mut()
                .expect("authorization session message data not available");

            // Header indexes start at index 1 (not 0).
            message.auth_results_i += 1;

            if let Some(incoming_aid) = auth_results::extract_authserv_id(value) {
                let aid = authserv_id(&self.config, self.conn.hostname());
                if eq_authserv_ids(aid, &incoming_aid) {
                    // We have a match. Remember the index position in order to
                    // delete this header at the `eom` stage.
                    debug!(
                        "{}: recognized incoming Authentication-Results header at index {}",
                        id, message.auth_results_i
                    );
                    message.auth_results_deletions.push(message.auth_results_i);
                }
            }
        }
    }

    /// Finishes the authorisation process for a message by consuming the
    /// message-scoped data, applying outstanding changes to the message header.
    pub fn finish_message(
        &mut self,
        context: &impl ActionContext,
        id: &str,
    ) -> milter::Result<Status> {
        let message = self
            .message
            .take()
            .expect("authorization session message data not available");

        if self.config.delete_incoming_authentication_results() {
            delete_auth_results_headers(context, &self.config, id, message.auth_results_deletions)?;
        }

        add_headers(
            context,
            &self.config,
            id,
            self.conn.hostname(),
            self.conn.ip(),
            self.conn.helo_host.as_deref(),
            message.results,
        )?;

        Ok(Status::Accept)
    }

    /// Clears this session’s message-scoped data.
    pub fn abort_message(&mut self) {
        self.message = None;
    }
}

fn skip_sender(identity: &str) -> milter::Result<Status> {
    debug!("not verifying exempt sender \"{}\"", identity);
    Ok(Status::Continue)
}

fn log_verification_result(result: &VerificationResult, repeated: bool) {
    let VerificationResult { identity, spf_result, .. } = result;
    if repeated {
        // Repeated identity verification results are demoted to `debug` level;
        // this is to avoid potentially confusing duplicate log lines.
        debug!(
            "{} ({}): {} (repeated verification result)",
            identity, identity.name(), spf_result.kind()
        );
    } else {
        info!("{} ({}): {}", identity, identity.name(), spf_result.kind());
    }
}

fn reject_sender(
    context: &impl SetErrorReply,
    config: &Config,
    scope: &str,
    result: &VerificationResult,
) -> milter::Result<Status> {
    let VerificationResult { identity, spf_result, .. } = result;

    if config.dry_run() {
        debug!("rejected {} from sender \"{}\" [dry run, not done]", scope, identity);
        Ok(Status::Accept)
    } else {
        let (reply_code, status_code, reply_text) = to_reply_params(config, spf_result);
        let reply_text = escape_reply_text(reply_text);

        context.set_error_reply(
            reply_code.as_ref(),
            Some(status_code.as_ref()),
            vec![&reply_text],  // multiline replies not supported
        )?;

        debug!("rejected {} from sender \"{}\"", scope, identity);
        Ok(match reply_code {
            ReplyCode::Transient(_) => Status::Tempfail,
            ReplyCode::Permanent(_) => Status::Reject,
        })
    }
}

fn to_reply_params<'a>(
    config: &'a Config,
    spf_result: &'a SpfResult,
) -> (&'a ReplyCode, &'a EnhancedStatusCode, &'a str) {
    use SpfResult::*;
    match spf_result {
        Fail(e) => (
            config.fail_reply_code(),
            config.fail_status_code(),
            e.as_ref(),
        ),
        Softfail => (
            config.softfail_reply_code(),
            config.softfail_status_code(),
            config.softfail_reply_text(),
        ),
        Temperror => (
            config.temperror_reply_code(),
            config.temperror_status_code(),
            config.temperror_reply_text(),
        ),
        Permerror => (
            config.permerror_reply_code(),
            config.permerror_status_code(),
            config.permerror_reply_text(),
        ),
        _ => panic!("rejection of SPF result \"{}\" not supported", spf_result),
    }
}

// The configuration may supply arbitrary reply text strings. According to
// libmilter API documentation, `%` must be escaped, and `\r` and `\n` are not
// allowed. Sanitise input for safe use as reply text.
fn escape_reply_text(s: &str) -> Cow<'_, str> {
    fn is_regular_char(c: char) -> bool {
        c.is_ascii_graphic() && !matches!(c, '%') || matches!(c, ' ' | '\t') || !c.is_ascii()
    }

    if s.chars().all(is_regular_char) {
        s.into()
    } else {
        let mut result = String::with_capacity(s.len());
        for c in s.chars() {
            if is_regular_char(c) {
                result.push(c);
            } else if c == '%' {
                result.push_str("%%");
            } else {
                result.extend(c.escape_default());
            }
        }
        result.into()
    }
}

// See RFC 8601, section 5.
fn eq_authserv_ids(id1: &str, id2: &str) -> bool {
    fn to_unicode(s: &str) -> String {
        let (result, e) = idna::domain_to_unicode(s);
        if e.is_err() {
            debug!("validation error while converting domain \"{}\" to Unicode", s);
        }
        result
    }

    to_unicode(id1) == to_unicode(id2)
}

fn delete_auth_results_headers<I>(
    context: &impl ActionContext,
    config: &Config,
    id: &str,
    deletions: I,
) -> milter::Result<()>
where
    I: IntoIterator<Item = usize>,
{
    for i in deletions {
        // Log at `info` instead of `debug` level, because unlike other actions
        // header deletion is invisible and may be considered significant.
        if config.dry_run() {
            info!("{}: deleting incoming Authentication-Results header at index {} [dry run, not done]", id, i);
        } else {
            info!("{}: deleting incoming Authentication-Results header at index {}", id, i);
            context.replace_header(AuthenticationResultsHeader::NAME, i, None)?;
        }
    }
    Ok(())
}

fn add_headers(
    context: &impl ActionContext,
    config: &Config,
    id: &str,
    hostname: &str,
    ip: IpAddr,
    helo_host: Option<&str>,
    results: Vec<VerificationResult>,
) -> milter::Result<()> {
    // Iterate in reverse: if more than a single header is to be added, the
    // resulting message reflects the order given in configuration.
    for header_type in config.header().iter().rev() {
        match header_type {
            HeaderType::ReceivedSpf => {
                for result in &results {
                    let header = ReceivedSpfHeader::new(result, hostname, ip, helo_host);
                    insert_header_field(context, config, id, header)?;
                }
            }
            HeaderType::AuthenticationResults => {
                if !results.is_empty() {
                    let authserv_id = authserv_id(config, hostname);
                    let header = AuthenticationResultsHeader::new(
                        authserv_id,
                        &results,
                        config.include_mailfrom_local_part(),
                    );
                    insert_header_field(context, config, id, header)?;
                }
            }
        }
    }

    Ok(())
}

fn insert_header_field(
    context: &impl ActionContext,
    config: &Config,
    id: &str,
    header: impl ToHeaderField,
) -> milter::Result<()> {
    let header_field = header.to_header_field();

    let header_name = header_field.name();

    if config.dry_run() {
        debug!("{}: adding {} header [dry run, not done]", id, header_name);
        debug!("{}: {}", id, header_field);
    } else {
        debug!("{}: adding {} header", id, header_name);
        debug!("{}: {}", id, header_field);
        context.insert_header(0, header_name, &header_field.format_body())?;
    }

    Ok(())
}

fn authserv_id<'a>(config: &'a Config, hostname: &'a str) -> &'a str {
    config.authserv_id().unwrap_or(hostname)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::config::model::{DefinitiveHeloResultKind, RejectResultKind, SkipEntry};
    use milter::{ActionContext, SetErrorReply};
    use std::{
        cell::RefCell,
        collections::HashSet,
        net::{Ipv4Addr, Ipv6Addr},
        rc::Rc,
    };
    use viaspf::{Lookup, LookupError, LookupResult, Name};

    #[test]
    fn escape_reply_text_ok() {
        assert_eq!(escape_reply_text("a b%20c"), "a b%%20c");
        assert_eq!(escape_reply_text("a\rb"), "a\\rb");
        assert_eq!(escape_reply_text("a🟥b"), "a🟥b");
    }

    #[test]
    fn eq_authserv_ids_ok() {
        assert!(eq_authserv_ids("example.org", "eXaMpLe.OrG"));
        assert!(eq_authserv_ids("ÖBB.at", "xn--bb-eka.at"));
        assert!(!eq_authserv_ids("oebb.at", "xn--bb-eka.at"));
    }

    #[derive(Default)]
    struct MockContext {
        insert_header: Option<Box<dyn Fn(usize, &str, &str) -> milter::Result<()> + 'static>>,
        replace_header: Option<Box<dyn Fn(&str, usize, Option<&str>) -> milter::Result<()> + 'static>>,
        set_error_reply: Option<Box<dyn Fn(&str, Option<&str>, Vec<&str>) -> milter::Result<()> + 'static>>,
    }

    impl ActionContext for MockContext {
        fn insert_header(&self, index: usize, name: &str, value: &str) -> milter::Result<()> {
            self.insert_header.as_ref().map_or(Ok(()), |f| f(index, name, value))
        }

        fn replace_header(&self, name: &str, index: usize, value: Option<&str>) -> milter::Result<()> {
            self.replace_header.as_ref().map_or(Ok(()), |f| f(name, index, value))
        }

        fn replace_sender(&self, _: &str, _: Option<&str>) -> milter::Result<()> { unimplemented!() }
        fn add_recipient(&self, _: &str, _: Option<&str>) -> milter::Result<()> { unimplemented!() }
        fn remove_recipient(&self, _: &str) -> milter::Result<()> { unimplemented!() }
        fn add_header(&self, _: &str, _: &str) -> milter::Result<()> { unimplemented!() }
        fn append_body_chunk(&self, _: &[u8]) -> milter::Result<()> { unimplemented!() }
        fn quarantine(&self, _: &str) -> milter::Result<()> { unimplemented!() }
        fn signal_progress(&self) -> milter::Result<()> { unimplemented!() }
    }

    impl SetErrorReply for MockContext {
        fn set_error_reply(&self, code: &str, ext_code: Option<&str>, msg_lines: Vec<&str>) -> milter::Result<()> {
            self.set_error_reply.as_ref().map_or(Ok(()), |f| f(code, ext_code, msg_lines))
        }
    }

    #[derive(Default)]
    struct MockLookup {
        lookup_a: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<Ipv4Addr>> + Send + Sync + 'static>>,
        lookup_aaaa: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<Ipv6Addr>> + Send + Sync + 'static>>,
        lookup_mx: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<Name>> + Send + Sync + 'static>>,
        lookup_txt: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<String>> + Send + Sync + 'static>>,
        lookup_ptr: Option<Box<dyn Fn(IpAddr) -> LookupResult<Vec<Name>> + Send + Sync + 'static>>,
    }

    impl Lookup for MockLookup {
        fn lookup_a(&self, name: &Name) -> LookupResult<Vec<Ipv4Addr>> {
            self.lookup_a.as_ref().map_or(Err(LookupError::NoRecords), |f| f(name))
        }

        fn lookup_aaaa(&self, name: &Name) -> LookupResult<Vec<Ipv6Addr>> {
            self.lookup_aaaa.as_ref().map_or(Err(LookupError::NoRecords), |f| f(name))
        }

        fn lookup_mx(&self, name: &Name) -> LookupResult<Vec<Name>> {
            self.lookup_mx.as_ref().map_or(Err(LookupError::NoRecords), |f| f(name))
        }

        fn lookup_txt(&self, name: &Name) -> LookupResult<Vec<String>> {
            self.lookup_txt.as_ref().map_or(Err(LookupError::NoRecords), |f| f(name))
        }

        fn lookup_ptr(&self, ip: IpAddr) -> LookupResult<Vec<Name>> {
            self.lookup_ptr.as_ref().map_or(Err(LookupError::NoRecords), |f| f(ip))
        }
    }

    #[derive(Clone, Debug, Eq, Hash, PartialEq)]
    struct InsertedHeader {
        name: String,
        value: String,
    }

    impl InsertedHeader {
        fn new(name: impl Into<String>, value: impl Into<String>) -> Self {
            Self {
                name: name.into(),
                value: value.into(),
            }
        }
    }

    impl From<(usize, &str, &str)> for InsertedHeader {
        fn from((index, name, value): (usize, &str, &str)) -> Self {
            assert_eq!(index, 0);
            Self::new(name, value)
        }
    }

    #[derive(Clone, Debug, Eq, Hash, PartialEq)]
    struct DeletedHeader {
        name: String,
        index: usize,
    }

    impl DeletedHeader {
        fn new(name: impl Into<String>, index: usize) -> Self {
            Self {
                name: name.into(),
                index,
            }
        }
    }

    impl From<(&str, usize, Option<&str>)> for DeletedHeader {
        fn from((name, index, value): (&str, usize, Option<&str>)) -> Self {
            assert_eq!(value, None);
            Self::new(name, index)
        }
    }

    #[derive(Clone, Debug, Eq, Hash, PartialEq)]
    struct ErrorReply {
        code: String,
        ext_code: String,
        msg: String,
    }

    impl ErrorReply {
        fn new(code: impl Into<String>, ext_code: impl Into<String>, msg: impl Into<String>) -> Self {
            Self {
                code: code.into(),
                ext_code: ext_code.into(),
                msg: msg.into(),
            }
        }
    }

    impl From<(&str, Option<&str>, Vec<&str>)> for ErrorReply {
        fn from((code, ext_code, msg_lines): (&str, Option<&str>, Vec<&str>)) -> Self {
            assert_eq!(msg_lines.len(), 1);
            let msg = msg_lines.into_iter().next().unwrap();
            Self::new(code, ext_code.unwrap(), msg)
        }
    }

    const SOCKET: &str = "unused";
    const ID: &str = "NONE";

    #[test]
    fn reject_unauthorized_sender() {
        let config = Config::builder(SOCKET)
            .verify_helo(false)
            .reject_results({
                let mut s = HashSet::new();
                s.insert(RejectResultKind::Softfail);
                s
            })
            .softfail_reply_code("551".parse().unwrap())
            .softfail_status_code("5.1.1".parse().unwrap())
            .softfail_reply_text("not authorized")
            .build()
            .unwrap();

        let mut session = AuthSession::new(config.into());

        session.init_connection("mail.example.org", [1, 2, 3, 4]);

        let error_reply = Rc::new(RefCell::new(None));
        let context = {
            let error_reply = Rc::clone(&error_reply);
            MockContext {
                set_error_reply: Some(Box::new(move |code, status, text| {
                    *error_reply.borrow_mut() = Some((code, status, text).into());
                    Ok(())
                })),
                ..Default::default()
            }
        };

        let resolver = MockResolver::new(MockLookup {
            lookup_txt: Some(Box::new(|name| match name.as_str() {
                "example.com." => Ok(vec!["v=spf1 ~all".into()]),
                _ => panic!(),
            })),
            ..Default::default()
        });

        let status = session.authorize_helo(&context, "mail.example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Continue);

        let status = session.authorize_mail_from(&context, "me@example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Reject);
        assert_eq!(
            *error_reply.borrow(),
            Some(ErrorReply::new("551", "5.1.1", "not authorized"))
        );
    }

    #[test]
    fn reject_null_sender_with_invalid_helo_record() {
        let config = Config::builder(SOCKET)
            .reject_helo_results(HashSet::new())
            .permerror_reply_text("permanent SPF error")
            .build()
            .unwrap();

        let mut session = AuthSession::new(config.into());

        session.init_connection("mail.example.org", [1, 2, 3, 4]);

        let error_reply = Rc::new(RefCell::new(None));
        let context = {
            let error_reply = Rc::clone(&error_reply);
            MockContext {
                set_error_reply: Some(Box::new(move |code, status, text| {
                    *error_reply.borrow_mut() = Some((code, status, text).into());
                    Ok(())
                })),
                ..Default::default()
            }
        };

        let resolver = MockResolver::new(MockLookup {
            lookup_txt: Some(Box::new(|name| match name.as_str() {
                "mail.example.com." => Ok(vec!["v=spf1 invalid record".into()]),
                _ => panic!(),
            })),
            ..Default::default()
        });

        let status = session.authorize_helo(&context, "mail.example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Continue);

        let status = session.authorize_mail_from(&context, "<>", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Reject);
        assert_eq!(
            *error_reply.borrow(),
            Some(ErrorReply::new("550", "5.7.24", "permanent SPF error"))
        );
    }

    #[test]
    fn reject_helo_timeout_with_escaped_reply_text() {
        let config = Config::builder(SOCKET)
            .temperror_reply_code("441".parse().unwrap())
            .temperror_status_code("4.1.1".parse().unwrap())
            .temperror_reply_text("multiline\nwith%20escaping")
            .build()
            .unwrap();

        let mut session = AuthSession::new(config.into());

        session.init_connection("mail.example.org", [1, 2, 3, 4]);

        let error_reply = Rc::new(RefCell::new(None));
        let context = {
            let error_reply = Rc::clone(&error_reply);
            MockContext {
                set_error_reply: Some(Box::new(move |code, status, text| {
                    *error_reply.borrow_mut() = Some((code, status, text).into());
                    Ok(())
                })),
                ..Default::default()
            }
        };

        let resolver = MockResolver::new(MockLookup {
            lookup_txt: Some(Box::new(|name| match name.as_str() {
                "mail.example.com." => Err(LookupError::Timeout),
                _ => panic!(),
            })),
            ..Default::default()
        });

        let status = session.authorize_helo(&context, "mail.example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Tempfail);
        assert_eq!(
            *error_reply.borrow(),
            Some(ErrorReply::new("441", "4.1.1", "multiline\\nwith%%20escaping"))
        );
    }

    #[test]
    fn reject_failure_with_i18n_explanation_from_dns() {
        let config = Config::builder(SOCKET)
            .verify_helo(false)
            .build()
            .unwrap();

        let mut session = AuthSession::new(config.into());

        session.init_connection("mail.example.org", [1, 2, 3, 4]);

        let error_reply = Rc::new(RefCell::new(None));
        let context = {
            let error_reply = Rc::clone(&error_reply);
            MockContext {
                set_error_reply: Some(Box::new(move |code, status, text| {
                    *error_reply.borrow_mut() = Some((code, status, text).into());
                    Ok(())
                })),
                ..Default::default()
            }
        };

        let resolver = MockResolver::new(MockLookup {
            lookup_txt: Some(Box::new(|name| match name.as_str() {
                "example.com." => Ok(vec!["v=spf1 redirect=explainer.org".into()]),
                "explainer.org." => Ok(vec!["v=spf1 -all exp=exp._spf.%{d}".into()]),
                "exp._spf.explainer.org." => Ok(vec!["You, %{l}, are 100%% a fraud!".into()]),
                _ => panic!(),
            })),
            ..Default::default()
        });

        let status = session.authorize_helo(&context, "mail.example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Continue);

        let status = session.authorize_mail_from(&context, "敦文@example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Reject);
        assert_eq!(
            *error_reply.borrow(),
            Some(ErrorReply::new(
                "550",
                "5.7.23",
                "SPF validation failed: example.com explains: You, 敦文, are 100%% a fraud!",
            ))
        );
    }

    #[test]
    fn delete_forged_authentication_results_headers() {
        let config = Config::builder(SOCKET)
            .delete_incoming_authentication_results(true)
            .build()
            .unwrap();

        let mut session = AuthSession::new(config.into());

        session.init_connection("mail.example.org", [1, 2, 3, 4]);

        let context = MockContext::default();

        let resolver = MockResolver::new(MockLookup::default());

        let status = session.authorize_helo(&context, "mail.example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Continue);

        let status = session.authorize_mail_from(&context, "me@example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Continue);

        session.process_auth_results_header(ID, "mail.example.org; spf=pass");
        session.process_auth_results_header(ID, "example.org; spf=neutral");
        session.process_auth_results_header(ID, "\"mail.EXAMPLE.org\"; spf=pass");

        let deleted_headers = Rc::new(RefCell::new(Vec::<DeletedHeader>::new()));
        let context = {
            let deleted_headers = Rc::clone(&deleted_headers);
            MockContext {
                replace_header: Some(Box::new(move |name, index, value| {
                    deleted_headers.borrow_mut().push((name, index, value).into());
                    Ok(())
                })),
                ..Default::default()
            }
        };

        let status = session.finish_message(&context, ID);

        assert_eq!(status.unwrap(), Status::Accept);
        assert_eq!(
            *deleted_headers.borrow(),
            vec![
                DeletedHeader::new("Authentication-Results", 1),
                DeletedHeader::new("Authentication-Results", 3),
            ]
        );
    }

    #[test]
    fn add_header_default_no_spf() {
        let config = Config::builder(SOCKET).build().unwrap();

        let mut session = AuthSession::new(config.into());

        session.init_connection("mail.example.org", [1, 2, 3, 4]);

        let context = MockContext::default();

        let resolver = MockResolver::new(MockLookup::default());

        let status = session.authorize_helo(&context, "mail.example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Continue);

        let status = session.authorize_mail_from(&context, "me@example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Continue);

        let inserted_header = Rc::new(RefCell::new(None));
        let context = {
            let inserted_header = Rc::clone(&inserted_header);
            MockContext {
                insert_header: Some(Box::new(move |index, name, value| {
                    *inserted_header.borrow_mut() = Some((index, name, value).into());
                    Ok(())
                })),
                ..Default::default()
            }
        };

        let status = session.finish_message(&context, ID);

        assert_eq!(status.unwrap(), Status::Accept);
        assert_eq!(
            *inserted_header.borrow(),
            Some(InsertedHeader::new(
                "Received-SPF",
                "none (mail.example.org: no authorization information available\n\
                \tfor sender me@example.com) receiver=mail.example.org; client-ip=1.2.3.4;\n\
                \thelo=mail.example.com; envelope-from=\"me@example.com\"; identity=mailfrom"
            ))
        );
    }

    #[test]
    fn add_header_for_definitive_helo_result() {
        let config = Config::builder(SOCKET)
            .verify_helo(true)
            .definitive_helo_results({
                let mut s = HashSet::new();
                s.insert(DefinitiveHeloResultKind::Pass);
                s
            })
            .header(HeaderType::AuthenticationResults)
            .build()
            .unwrap();

        let mut session = AuthSession::new(config.into());

        session.init_connection("mail.example.org", [1, 2, 3, 4]);

        let context = MockContext::default();

        let resolver = MockResolver::new(MockLookup {
            lookup_txt: Some(Box::new(|name| match name.as_str() {
                "mail.example.com." => Ok(vec!["v=spf1 +all".into()]),
                _ => panic!(),
            })),
            ..Default::default()
        });

        let status = session.authorize_helo(&context, "mail.example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Continue);

        let status = session.authorize_mail_from(&context, "me@example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Continue);

        let inserted_header = Rc::new(RefCell::new(None));
        let context = {
            let inserted_header = Rc::clone(&inserted_header);
            MockContext {
                insert_header: Some(Box::new(move |index, name, value| {
                    *inserted_header.borrow_mut() = Some((index, name, value).into());
                    Ok(())
                })),
                ..Default::default()
            }
        };

        let status = session.finish_message(&context, ID);

        assert_eq!(status.unwrap(), Status::Accept);
        assert_eq!(
            *inserted_header.borrow(),
            Some(InsertedHeader::new(
                "Authentication-Results",
                "mail.example.org; spf=pass smtp.helo=mail.example.com",
            ))
        );
    }

    #[test]
    fn add_header_with_unusable_helo_identity() {
        let config = Config::builder(SOCKET)
            .verify_helo(true)
            .build()
            .unwrap();

        let mut session = AuthSession::new(config.into());

        session.init_connection("mail.example.org", [1, 2, 3, 4]);

        let context = MockContext::default();

        let resolver = MockResolver::new(MockLookup {
            lookup_txt: Some(Box::new(|name| match name.as_str() {
                "example.com." => Ok(vec!["v=spf1 +all".into()]),
                _ => panic!(),
            })),
            ..Default::default()
        });

        let status = session.authorize_helo(&context, "[1.2.3.4]", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Continue);

        let status = session.authorize_mail_from(&context, "me@example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Continue);

        let inserted_header = Rc::new(RefCell::new(None));
        let context = {
            let inserted_header = Rc::clone(&inserted_header);
            MockContext {
                insert_header: Some(Box::new(move |index, name, value| {
                    *inserted_header.borrow_mut() = Some((index, name, value).into());
                    Ok(())
                })),
                ..Default::default()
            }
        };

        let status = session.finish_message(&context, ID);

        assert_eq!(status.unwrap(), Status::Accept);
        assert_eq!(
            *inserted_header.borrow(),
            Some(InsertedHeader::new(
                "Received-SPF",
                "pass (mail.example.org: domain of me@example.com has authorized\n\
                \thost 1.2.3.4) receiver=mail.example.org; client-ip=1.2.3.4; helo=\"[1.2.3.4]\";\n\
                \tenvelope-from=\"me@example.com\"; identity=mailfrom; mechanism=all"
            ))
        );
    }

    #[test]
    fn add_headers_for_all_results() {
        let config = Config::builder(SOCKET)
            .header(vec![HeaderType::AuthenticationResults, HeaderType::ReceivedSpf])
            .include_all_results(true)
            .build()
            .unwrap();

        let mut session = AuthSession::new(config.into());

        session.init_connection("mail.example.org", [1, 2, 3, 4]);

        let context = MockContext::default();

        let resolver = MockResolver::new(MockLookup {
            lookup_txt: Some(Box::new(|_| Ok(vec!["v=spf1 +all".into()]))),
            ..Default::default()
        });

        let status = session.authorize_helo(&context, "mail.example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Continue);

        let status = session.authorize_mail_from(&context, "me@example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Continue);

        let inserted_headers = Rc::new(RefCell::new(Vec::<InsertedHeader>::new()));
        let context = {
            let inserted_headers = Rc::clone(&inserted_headers);
            MockContext {
                insert_header: Some(Box::new(move |index, name, value| {
                    inserted_headers.borrow_mut().push((index, name, value).into());
                    Ok(())
                })),
                ..Default::default()
            }
        };

        let status = session.finish_message(&context, ID);

        assert_eq!(status.unwrap(), Status::Accept);
        assert_eq!(
            *inserted_headers.borrow(),
            vec![
                InsertedHeader::new(
                    "Received-SPF",
                    "pass (mail.example.org: domain mail.example.com has authorized\n\
                    \thost 1.2.3.4) receiver=mail.example.org; client-ip=1.2.3.4;\n\
                    \thelo=mail.example.com; identity=helo; mechanism=all"
                ),
                InsertedHeader::new(
                    "Received-SPF",
                    "pass (mail.example.org: domain of me@example.com has authorized\n\
                    \thost 1.2.3.4) receiver=mail.example.org; client-ip=1.2.3.4;\n\
                    \thelo=mail.example.com; envelope-from=\"me@example.com\"; identity=mailfrom;\n\
                    \tmechanism=all"
                ),
                InsertedHeader::new(
                    "Authentication-Results",
                    "mail.example.org; spf=pass smtp.helo=mail.example.com;\n\
                    \tspf=pass smtp.mailfrom=example.com"
                ),
            ]
        );
    }

    #[test]
    fn skip_sender_matching_mailfrom_identity() {
        let config = Config::builder(SOCKET)
            .verify_helo(false)
            .skip_senders({
                let mut s = HashSet::new();
                s.insert(SkipEntry {
                    local_part: None,
                    domain: Name::domain("EXAMPLE.COM").unwrap(),
                    match_subdomains: false,
                });
                s
            })
            .build()
            .unwrap();

        let mut session = AuthSession::new(config.into());

        session.init_connection("mail.example.org", [1, 2, 3, 4]);

        let context = MockContext::default();

        let resolver = MockResolver::new(MockLookup {
            lookup_txt: Some(Box::new(|_| panic!())),
            ..Default::default()
        });

        let status = session.authorize_helo(&context, "mail.example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Continue);

        let status = session.authorize_mail_from(&context, "me@example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Continue);

        let context = MockContext {
            insert_header: Some(Box::new(|_, _, _| panic!())),
            ..Default::default()
        };

        let status = session.finish_message(&context, ID);

        assert_eq!(status.unwrap(), Status::Accept);
    }

    #[test]
    fn dry_run_accepts_message() {
        let config = Config::builder(SOCKET)
            .dry_run(true)
            .build()
            .unwrap();

        let mut session = AuthSession::new(config.into());

        session.init_connection("mail.example.org", [1, 2, 3, 4]);

        let context = MockContext {
            set_error_reply: Some(Box::new(|_, _, _| panic!())),
            ..Default::default()
        };

        let resolver = MockResolver::new(MockLookup {
            lookup_txt: Some(Box::new(|_| Ok(vec!["v=spf1 -all".into()]))),
            ..Default::default()
        });

        let status = session.authorize_helo(&context, "mail.example.com", Some(&resolver));

        assert_eq!(status.unwrap(), Status::Accept);
    }
}
