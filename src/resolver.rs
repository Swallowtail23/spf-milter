use domain::{
    base::{Dname, Rtype},
    rdata::{Aaaa, Mx, Ptr, Txt, A},
    resolv::{
        stub::conf::{ResolvConf, ResolvOptions},
        StubResolver,
    },
};
use once_cell::sync::OnceCell;
use std::{
    error::Error,
    io::{self, ErrorKind},
    net::{IpAddr, Ipv4Addr, Ipv6Addr},
    str::FromStr,
    time::Duration,
};
use viaspf::{Lookup, LookupError, LookupResult, Name};

static MOCK_RESOLVER: OnceCell<Option<MockResolver>> = OnceCell::new();

/// Initialises the static mock resolver, which may be `None`.
///
/// Must be called exactly once during startup.
pub fn init_mock(mock_resolver: Option<MockResolver>) {
    // Cannot use `expect` because the mock resolver does not implement `Debug`.
    if MOCK_RESOLVER.set(mock_resolver).is_err() {
        panic!("mock resolver already initialized");
    }
}

/// Returns a reference to the static mock resolver, if present.
pub fn get_mock() -> Option<&'static MockResolver> {
    MOCK_RESOLVER.get().expect("mock resolver not initialized").as_ref()
}

/// A resolver wrapping a mock `Lookup`.
///
/// For use in testing.
pub struct MockResolver(Box<dyn Lookup + Send + Sync + 'static>);

impl MockResolver {
    pub fn new(lookup: impl Lookup + Send + Sync + 'static) -> Self {
        Self(Box::new(lookup))
    }
}

impl Lookup for MockResolver {
    fn lookup_a(&self, name: &Name) -> LookupResult<Vec<Ipv4Addr>> {
        self.0.lookup_a(name)
    }

    fn lookup_aaaa(&self, name: &Name) -> LookupResult<Vec<Ipv6Addr>> {
        self.0.lookup_aaaa(name)
    }

    fn lookup_mx(&self, name: &Name) -> LookupResult<Vec<Name>> {
        self.0.lookup_mx(name)
    }

    fn lookup_txt(&self, name: &Name) -> LookupResult<Vec<String>> {
        self.0.lookup_txt(name)
    }

    fn lookup_ptr(&self, ip: IpAddr) -> LookupResult<Vec<Name>> {
        self.0.lookup_ptr(ip)
    }
}

/// A resolver doing live DNS queries.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct DomainResolver {
    timeout: Duration,
}

impl DomainResolver {
    pub fn new(timeout: Duration) -> Self {
        Self { timeout }
    }

    fn make_resolv_conf(&self) -> ResolvConf {
        let mut conf = ResolvConf::default();
        let options = ResolvOptions {
            timeout: self.timeout,
            attempts: 1,
            ..Default::default()
        };
        conf.options = options;
        conf.finalize();
        conf
    }
}

impl Lookup for DomainResolver {
    fn lookup_a(&self, name: &Name) -> LookupResult<Vec<Ipv4Addr>> {
        let name = to_dname(name)?;
        let answer =
            StubResolver::run_with_conf(self.make_resolv_conf(), move |resolver| async move {
                resolver.query((name, Rtype::A)).await
            })
            .map_err(to_lookup_error)?;
        answer
            .answer()
            .map_err(wrap_error)?
            .limit_to::<A>()
            .map(|record| record.map(|r| r.data().addr()))
            .collect::<Result<Vec<_>, _>>()
            .map_err(wrap_error)
    }

    fn lookup_aaaa(&self, name: &Name) -> LookupResult<Vec<Ipv6Addr>> {
        let name = to_dname(name)?;
        let answer =
            StubResolver::run_with_conf(self.make_resolv_conf(), move |resolver| async move {
                resolver.query((name, Rtype::Aaaa)).await
            })
            .map_err(to_lookup_error)?;
        answer
            .answer()
            .map_err(wrap_error)?
            .limit_to::<Aaaa>()
            .map(|record| record.map(|r| r.data().addr()))
            .collect::<Result<Vec<_>, _>>()
            .map_err(wrap_error)
    }

    fn lookup_mx(&self, name: &Name) -> LookupResult<Vec<Name>> {
        let name = to_dname(name)?;
        let answer =
            StubResolver::run_with_conf(self.make_resolv_conf(), move |resolver| async move {
                resolver.query((name, Rtype::Mx)).await
            })
            .map_err(to_lookup_error)?;
        let mut mxs = answer
            .answer()
            .map_err(wrap_error)?
            .limit_to::<Mx<_>>()
            .map(|record| record.map(|r| r.into_data()))
            .collect::<Result<Vec<_>, _>>()
            .map_err(wrap_error)?;
        mxs.sort_by_key(|mx| mx.preference());
        mxs.into_iter()
            .map(|mx| Name::new(&mx.exchange().to_string()))
            .collect::<Result<Vec<_>, _>>()
            .map_err(wrap_error)
    }

    fn lookup_txt(&self, name: &Name) -> LookupResult<Vec<String>> {
        let name = to_dname(name)?;
        let answer =
            StubResolver::run_with_conf(self.make_resolv_conf(), move |resolver| async move {
                resolver.query((name, Rtype::Txt)).await
            })
            .map_err(to_lookup_error)?;
        let txts = answer
            .answer()
            .map_err(wrap_error)?
            .limit_to::<Txt<_>>()
            .map(|record| record.map(|r| r.into_data()))
            .collect::<Result<Vec<_>, _>>()
            .map_err(wrap_error)?;
        txts.into_iter()
            .map(|txt| {
                txt.text::<Vec<_>>()
                    .map(|bytes| String::from_utf8_lossy(&bytes).into_owned())
            })
            .collect::<Result<Vec<_>, _>>()
            .map_err(wrap_error)
    }

    fn lookup_ptr(&self, ip: IpAddr) -> LookupResult<Vec<Name>> {
        let name = ip_to_dname(ip)?;
        let answer =
            StubResolver::run_with_conf(self.make_resolv_conf(), move |resolver| async move {
                resolver.query((name, Rtype::Ptr)).await
            })
            .map_err(to_lookup_error)?;
        let ptrs = answer
            .answer()
            .map_err(wrap_error)?
            .limit_to::<Ptr<_>>()
            .map(|record| record.map(|r| r.into_data()))
            .collect::<Result<Vec<_>, _>>()
            .map_err(wrap_error)?;
        ptrs.into_iter()
            .map(|ptr| Name::new(&ptr.to_string()))
            .collect::<Result<Vec<_>, _>>()
            .map_err(wrap_error)
    }
}

fn to_dname(name: &Name) -> LookupResult<Dname<Vec<u8>>> {
    Dname::from_str(name.as_str()).map_err(wrap_error)
}

fn ip_to_dname(ip: IpAddr) -> LookupResult<Dname<Vec<u8>>> {
    let s = match ip {
        IpAddr::V4(addr) => {
            let [a, b, c, d] = addr.octets();
            format!("{}.{}.{}.{}.in-addr.arpa.", d, c, b, a)
        }
        IpAddr::V6(addr) => format!(
            "{}.ip6.arpa.",
            addr.octets()
                .iter()
                .rev()
                .map(|o| format!("{:x}.{:x}", o & 0xf, o >> 4))
                .collect::<Vec<_>>()
                .join(".")
        ),
    };
    Dname::from_str(&s).map_err(wrap_error)
}

fn to_lookup_error(error: io::Error) -> LookupError {
    match error.kind() {
        ErrorKind::NotFound => LookupError::NoRecords,
        ErrorKind::TimedOut => LookupError::Timeout,
        _ => wrap_error(error),
    }
}

fn wrap_error(error: impl Error + Send + Sync + 'static) -> LookupError {
    LookupError::Dns(Some(error.into()))
}

#[cfg(test)]
mod tests {
    use super::*;

    // This test is disabled because it depends on live DNS records.
    #[ignore]
    #[test]
    fn domain_resolver_lookup_ok() {
        let resolver = DomainResolver::new(Duration::from_secs(30));

        let domain = Name::new("gluet.ch").unwrap();

        let ips = resolver.lookup_a(&domain);
        assert!(ips.is_ok());
        let ip = ips.unwrap().into_iter().next().unwrap();
        assert!(resolver.lookup_ptr(ip.into()).is_ok());

        let ips = resolver.lookup_aaaa(&domain);
        assert!(ips.is_ok());
        let ip = ips.unwrap().into_iter().next().unwrap();
        assert!(resolver.lookup_ptr(ip.into()).is_ok());
    }
}
