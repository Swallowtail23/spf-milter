use crate::{
    auth::SpfResultKind,
    header::{format, FieldBody, FieldName, HeaderField, ToHeaderField},
    verify::{self, Identity, VerificationResult},
};
use std::borrow::Cow;
use viaspf::SpfResultCause;

// The `Authentication-Results` header field is defined in RFC 8601.

/// Extracts the *authserv-id* from an `Authentication-Results` header field
/// value, if found.
pub fn extract_authserv_id(value: &str) -> Option<Cow<'_, str>> {
    // *authserv-id* is a lexical token of kind `value` as defined in RFC 2045,
    // section 5.1, that is, either a `token` or a quoted string. Immediately
    // before the *authserv-id* there may be a CFWS.
    let value = format::skip_cfws(value).unwrap_or(value);

    if let Some(rest) = format::skip_mime_value(value) {
        // Directly after the *authserv-id* may come either a semicolon or
        // another CFWS. Validation proceeds no further than this.
        if rest.starts_with(';') || format::skip_cfws(rest).is_some() {
            // We have a match. If it is a quoted string, now it needs to be
            // decoded to be in a form comparable with another *authserv-id*.
            let authserv_id = &value[..(value.len() - rest.len())];
            return Some(if authserv_id.starts_with('"') {
                format::decode_quoted_string(authserv_id).into()
            } else {
                authserv_id.into()
            });
        }
    }

    None
}

/// A collection of data necessary for generating an `Authentication-Results`
/// header.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct AuthenticationResultsHeader<'a> {
    authserv_id: &'a str,
    results: &'a [VerificationResult],
    include_mailfrom_local_part: bool,
}

impl<'a> AuthenticationResultsHeader<'a> {
    pub const NAME: &'static str = "Authentication-Results";

    pub fn new(
        authserv_id: &'a str,
        results: &'a [VerificationResult],
        include_mailfrom_local_part: bool,
    ) -> Self {
        Self {
            authserv_id,
            results,
            include_mailfrom_local_part,
        }
    }
}

impl ToHeaderField for AuthenticationResultsHeader<'_> {
    fn to_header_field(&self) -> HeaderField {
        HeaderField {
            name: FieldName(Self::NAME),
            body: to_field_body(self),
        }
    }
}

fn to_field_body(header: &AuthenticationResultsHeader<'_>) -> FieldBody {
    let mut parts = make_auth_results_parts(header);

    if let [parts @ .., _] = &mut parts[..] {
        for part in parts {
            if let Some(last) = part.last_mut() {
                last.push(';');
            }
        }
    }

    FieldBody(parts.into_iter().flatten().collect())
}

fn make_auth_results_parts(header: &AuthenticationResultsHeader<'_>) -> Vec<Vec<String>> {
    let mut parts = Vec::new();

    parts.push(vec![format::encode_mime_value(header.authserv_id).into()]);

    for VerificationResult { identity, spf_result, cause } in header.results {
        let mut resinfos = Vec::new();

        resinfos.push(format!("spf={}", spf_result.kind()));

        // Only in the case of an error result do we record the result cause as
        // the ‘reason’.
        if let Some(SpfResultCause::Error(error_cause)) = cause {
            resinfos.push(format!(
                "reason={}",
                format::encode_mime_value(&error_cause.to_string())
            ));
        }

        resinfos.push(format!(
            "smtp.{}={}",
            identity.name(),
            encode_sender(&identity, header.include_mailfrom_local_part)
        ));

        parts.push(resinfos);
    }

    parts
}

fn encode_sender(identity: &Identity, include_mailfrom_local_part: bool) -> Cow<'_, str> {
    let mut sender = identity.as_ref();

    // The `Authentication-Results` header only records ‘authenticated’ data.
    // According to RFC 8601, even the MAIL FROM identity *should* therefore (by
    // default) only record the domain without the local-part.
    if let Identity::MailFrom(s) = identity {
        if !include_mailfrom_local_part {
            if let Some(i) = s.rfind('@') {
                sender = &s[i + 1..];
            }
        }
    };

    // Encode the sender identity as an RFC 2045 `value`, unless it conforms to
    // the production `[local-part "@"] domain-name` of RFC 8601, section 2.2,
    // in which case no quoting/encoding needs to be applied.
    if is_valid_unquoted_pvalue(sender) {
        sender.into()
    } else {
        format::encode_mime_value(sender)
    }
}

fn is_valid_unquoted_pvalue(mut s: &str) -> bool {
    // See RFC 5322, section 3.4.1.
    fn is_local_part(s: &str) -> bool {
        format::is_dot_atom(s) || format::is_quoted_string(s)
    }
    // See RFC 6376, section 3.5, and RFC 8616.
    fn is_domain_name(s: &str) -> bool {
        verify::is_fqdn(s)
    }

    if let Some(i) = s.rfind('@') {
        if !is_local_part(&s[..i]) {
            return false;
        }
        s = &s[i + 1..];
    }
    is_domain_name(s)
}

#[cfg(test)]
mod tests {
    use super::*;
    use viaspf::{
        record::{Mechanism, A},
        ErrorCause, SpfResult,
    };

    #[test]
    fn extract_authserv_id_ok() {
        assert_eq!(extract_authserv_id("x"), None);
        assert_eq!(extract_authserv_id("x;"), Some("x".into()));
        assert_eq!(extract_authserv_id("\"x\";"), Some("x".into()));
        assert_eq!(extract_authserv_id("x\n\t;"), Some("x".into()));
        assert_eq!(extract_authserv_id("xy.abc "), Some("xy.abc".into()));
        assert_eq!(
            extract_authserv_id(" (w h\n\tat ) x.34-q*y(\n\t());"),
            Some("x.34-q*y".into())
        );
        assert_eq!(
            extract_authserv_id(" (w h\n\tat ) \"x.34🚀-q?y\\ \"(\n\t());"),
            Some("x.34🚀-q?y ".into())
        );
    }

    #[test]
    fn encode_sender_ok() {
        use Identity::*;

        assert_eq!(encode_sender(&Helo("mail.gluet.ch".into()), false), "mail.gluet.ch");
        assert_eq!(encode_sender(&Helo("mail.gluet.ch.".into()), false), "mail.gluet.ch.");
        assert_eq!(encode_sender(&Helo("_mail.gluet.ch".into()), false), "_mail.gluet.ch");
        assert_eq!(encode_sender(&Helo("mail.例子.cn".into()), false), "mail.例子.cn");
        assert_eq!(encode_sender(&Helo(":unknown".into()), false), "\":unknown\"");

        assert_eq!(encode_sender(&MailFrom("me@localhost".into()), false), "localhost");
        assert_eq!(encode_sender(&MailFrom("me@例子.cn".into()), false), "例子.cn");

        assert_eq!(encode_sender(&MailFrom("me@例子.cn".into()), true), "me@例子.cn");
        assert_eq!(encode_sender(&MailFrom("me+you=♡@例子.cn".into()), true), "me+you=♡@例子.cn");
        assert_eq!(encode_sender(&MailFrom("\"me\"@例子.cn".into()), true), "\"me\"@例子.cn");
        assert_eq!(encode_sender(&MailFrom("me@例子".into()), true), "\"me@例子\"");
        assert_eq!(encode_sender(&MailFrom("me.@例子.cn".into()), true), "\"me.@例子.cn\"");
    }

    #[test]
    fn auth_results_to_header_field_display_error() {
        let results = vec![VerificationResult {
            identity: Identity::MailFrom("me@example.org".into()),
            spf_result: SpfResult::Permerror,
            cause: Some(SpfResultCause::Error(ErrorCause::LookupLimitExceeded)),
        }];

        let header = AuthenticationResultsHeader::new("mail.example.com", &results, false);

        assert_eq!(
            header.to_header_field().to_string(),
            "Authentication-Results: \
            mail.example.com; \
            spf=permerror \
            reason=\"lookup limit exceeded\" \
            smtp.mailfrom=example.org"
        );
    }

    #[test]
    fn auth_results_to_header_field_display_all_results() {
        let results = vec![
            VerificationResult {
                identity: Identity::Helo("mail.example.org".into()),
                spf_result: SpfResult::None,
                cause: None,
            },
            VerificationResult {
                identity: Identity::MailFrom("me@example.org".into()),
                spf_result: SpfResult::Pass,
                cause: Some(SpfResultCause::Match(Mechanism::A(A {
                    domain_spec: None,
                    prefix_len: None,
                }))),
            },
        ];

        let header = AuthenticationResultsHeader::new("mail.example.com", &results, true);

        assert_eq!(
            header.to_header_field().to_string(),
            "Authentication-Results: \
            mail.example.com; \
            spf=none smtp.helo=mail.example.org; \
            spf=pass smtp.mailfrom=me@example.org"
        );
    }
}
