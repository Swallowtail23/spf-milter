use crate::{
    auth::SpfResultKind,
    header::{format, FieldBody, FieldName, HeaderField, ToHeaderField},
    verify::{Identity, VerificationResult},
};
use std::{borrow::Cow, net::IpAddr};
use viaspf::{SpfResult, SpfResultCause};

/// A collection of data necessary for generating a `Received-SPF` header.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct ReceivedSpfHeader<'a> {
    result: &'a VerificationResult,
    hostname: &'a str,
    ip: IpAddr,
    helo_host: Option<&'a str>,
}

impl<'a> ReceivedSpfHeader<'a> {
    pub const NAME: &'static str = "Received-SPF";

    pub fn new(
        result: &'a VerificationResult,
        hostname: &'a str,
        ip: IpAddr,
        helo_host: Option<&'a str>,
    ) -> Self {
        Self {
            result,
            hostname,
            ip,
            helo_host,
        }
    }
}

impl ToHeaderField for ReceivedSpfHeader<'_> {
    fn to_header_field(&self) -> HeaderField {
        HeaderField {
            name: FieldName(Self::NAME),
            body: to_field_body(self),
        }
    }
}

fn to_field_body(header: &ReceivedSpfHeader<'_>) -> FieldBody {
    let mut parts = Vec::new();

    let VerificationResult { identity, spf_result, cause } = header.result;

    parts.push(spf_result.kind().into());

    add_comment(
        &mut parts,
        spf_result,
        header.hostname,
        header.ip,
        identity,
    );

    add_key_value_pairs(
        &mut parts,
        spf_result,
        cause.as_ref(),
        header.hostname,
        header.ip,
        header.helo_host.as_deref(),
        identity,
    );

    FieldBody(parts)
}

fn add_comment(
    parts: &mut Vec<String>,
    spf_result: &SpfResult,
    hostname: &str,
    ip: IpAddr,
    identity: &Identity,
) {
    use SpfResult::*;
    match spf_result {
        Pass => format_pass_parts(parts, hostname, ip, identity),
        Fail(_) => format_fail_parts(parts, hostname, ip, identity),
        Softfail => format_softfail_parts(parts, hostname, ip, identity),
        Neutral => format_neutral_parts(parts, hostname, ip, identity),
        None => format_none_parts(parts, hostname, identity),
        Temperror => format_temperror_parts(parts, hostname, identity),
        Permerror => format_permerror_parts(parts, hostname, identity),
    }
}

// (mail.example.org: domain [of me@]example.org has authorized host 1.2.3.4)
fn format_pass_parts(parts: &mut Vec<String>, hostname: &str, ip: IpAddr, identity: &Identity) {
    parts.push(format!("({}:", format::escape_comment_word(hostname)));
    format_sender(parts, identity);
    parts.extend("has authorized host".split_whitespace().map(From::from));
    parts.push(format!("{})", ip));
}

// (mail.example.org: domain [of me@]example.org has not authorized host 1.2.3.4)
fn format_fail_parts(parts: &mut Vec<String>, hostname: &str, ip: IpAddr, identity: &Identity) {
    parts.push(format!("({}:", format::escape_comment_word(hostname)));
    format_sender(parts, identity);
    parts.extend("has not authorized host".split_whitespace().map(From::from));
    parts.push(format!("{})", ip));
}

// (mail.example.org: domain [of me@]example.org discourages use of host 1.2.3.4)
fn format_softfail_parts(parts: &mut Vec<String>, hostname: &str, ip: IpAddr, identity: &Identity) {
    parts.push(format!("({}:", format::escape_comment_word(hostname)));
    format_sender(parts, identity);
    parts.extend("discourages use of host".split_whitespace().map(From::from));
    parts.push(format!("{})", ip));
}

// (mail.example.org: domain [of me@]example.org makes no definitive authorization statement for host 1.2.3.4)
fn format_neutral_parts(parts: &mut Vec<String>, hostname: &str, ip: IpAddr, identity: &Identity) {
    parts.push(format!("({}:", format::escape_comment_word(hostname)));
    format_sender(parts, identity);
    parts.extend("makes no definitive authorization statement for host".split_whitespace().map(From::from));
    parts.push(format!("{})", ip));
}

// (mail.example.org: no authorization information available for sender [me@]example.org)
fn format_none_parts(parts: &mut Vec<String>, hostname: &str, identity: &Identity) {
    parts.push(format!("({}:", format::escape_comment_word(hostname)));
    parts.extend("no authorization information available for sender".split_whitespace().map(From::from));
    parts.push(format!("{})", format::escape_comment_word(identity.as_ref())));
}

// (mail.example.org: sender [me@]example.org could not be authorized due to a transient DNS error)
fn format_temperror_parts(parts: &mut Vec<String>, hostname: &str, identity: &Identity) {
    parts.push(format!("({}:", format::escape_comment_word(hostname)));
    parts.push("sender".into());
    parts.push(format::escape_comment_word(identity.as_ref()).into());
    parts.extend("could not be authorized due to a transient DNS error)".split_whitespace().map(From::from));
}

// (mail.example.org: sender [me@]example.org could not be authorized due to a permanent error in SPF records)
fn format_permerror_parts(parts: &mut Vec<String>, hostname: &str, identity: &Identity) {
    parts.push(format!("({}:", format::escape_comment_word(hostname)));
    parts.push("sender".into());
    parts.push(format::escape_comment_word(identity.as_ref()).into());
    parts.extend("could not be authorized due to a permanent error in SPF records)".split_whitespace().map(From::from));
}

// HELO identity: ‘domain <sender>’, MAIL FROM identity: ‘domain of <sender>’
fn format_sender(parts: &mut Vec<String>, sender: &Identity) {
    parts.push("domain".into());
    if let Identity::MailFrom(_) = sender {
        parts.push("of".into());
    }
    parts.push(format::escape_comment_word(sender.as_ref()).into());
}

fn add_key_value_pairs(
    parts: &mut Vec<String>,
    spf_result: &SpfResult,
    cause: Option<&SpfResultCause>,
    hostname: &str,
    ip: IpAddr,
    helo_host: Option<&str>,
    identity: &Identity,
) {
    let kvs = make_key_value_pairs(spf_result, cause, hostname, ip, helo_host, identity);

    if let [kvs @ .., last] = &kvs[..] {
        for (key, value) in kvs {
            parts.push(format!("{}={};", key, format::encode_value(value)));
        }
        let (key, value) = last;
        parts.push(format!("{}={}", key, format::encode_value(value)));
    }
}

fn make_key_value_pairs<'a>(
    spf_result: &'a SpfResult,
    cause: Option<&'a SpfResultCause>,
    hostname: &'a str,
    ip: IpAddr,
    helo_host: Option<&'a str>,
    identity: &'a Identity,
) -> Vec<(&'static str, Cow<'a, str>)> {
    let mut kvs = Vec::new();

    kvs.push(("receiver", hostname.into()));
    kvs.push(("client-ip", ip.to_string().into()));

    // This key records the HELO name as given by the client. This may be
    // different from what was used for evaluation, for example when `unknown`
    // was substituted for an invalid HELO identity.
    if let Some(helo_host) = helo_host {
        kvs.push(("helo", helo_host.into()));
    }

    if let Identity::MailFrom(mail_from) = &identity {
        kvs.push(("envelope-from", mail_from.into()));
    }

    kvs.push(("identity", identity.name().into()));

    match cause {
        Some(SpfResultCause::Match(mechanism)) => {
            kvs.push(("mechanism", mechanism.to_string().into()));
        }
        Some(SpfResultCause::Error(error)) => {
            kvs.push(("problem", error.to_string().into()));
        }
        None => {
            if let SpfResult::Neutral = spf_result {
                // Neutral result, no mechanism matched: use value `default`.
                // See RFC 7208, section 9.1.
                kvs.push(("mechanism", "default".into()));
            }
        }
    }

    kvs
}

#[cfg(test)]
mod tests {
    use super::*;
    use viaspf::{
        record::{Ip4CidrLength, Mechanism, A},
        ErrorCause,
    };

    #[test]
    fn received_spf_to_header_field_display_pass() {
        let result = VerificationResult {
            identity: Identity::MailFrom("me@example.org".into()),
            spf_result: SpfResult::Pass,
            cause: Some(SpfResultCause::Match(Mechanism::A(A {
                domain_spec: None,
                prefix_len: Some(Ip4CidrLength::new(24).unwrap().into()),
            }))),
        };
        let header = ReceivedSpfHeader::new(
            &result,
            "mail.example.com",
            IpAddr::from([1, 2, 3, 4]),
            Some("mail.example.org"),
        );

        assert_eq!(
            header.to_header_field().to_string(),
            "Received-SPF: \
            pass \
            (mail.example.com: domain of me@example.org has authorized host 1.2.3.4) \
            receiver=mail.example.com; \
            client-ip=1.2.3.4; \
            helo=mail.example.org; \
            envelope-from=\"me@example.org\"; \
            identity=mailfrom; \
            mechanism=a/24"
        );
    }

    #[test]
    fn received_spf_to_header_field_display_error() {
        let result = VerificationResult {
            identity: Identity::Helo("mail.example.org".into()),
            spf_result: SpfResult::Temperror,
            cause: Some(SpfResultCause::Error(ErrorCause::Timeout)),
        };
        let header = ReceivedSpfHeader::new(
            &result,
            "mail.example.com",
            IpAddr::from([1, 2, 3, 4]),
            Some("mail.example.org"),
        );

        assert_eq!(
            header.to_header_field().to_string(),
            "Received-SPF: \
            temperror \
            (mail.example.com: sender mail.example.org could not be authorized due to a transient DNS error) \
            receiver=mail.example.com; \
            client-ip=1.2.3.4; \
            helo=mail.example.org; \
            identity=helo; \
            problem=\"DNS lookup timed out\""
        );
    }

    #[test]
    fn received_spf_to_header_field_display_default_neutral_result() {
        let result = VerificationResult {
            identity: Identity::MailFrom("me@example.org".into()),
            spf_result: SpfResult::Neutral,
            cause: None,
        };
        let header = ReceivedSpfHeader::new(
            &result,
            "mail.example.com",
            IpAddr::from([1, 2, 3, 4]),
            Some("mail.example.org"),
        );

        assert_eq!(
            header.to_header_field().to_string(),
            "Received-SPF: \
            neutral \
            (mail.example.com: domain of me@example.org makes no definitive authorization statement for host 1.2.3.4) \
            receiver=mail.example.com; \
            client-ip=1.2.3.4; \
            helo=mail.example.org; \
            envelope-from=\"me@example.org\"; \
            identity=mailfrom; \
            mechanism=default"
        );
    }

    #[test]
    fn format_pass_parts_ok() {
        let mut parts = Vec::new();
        format_pass_parts(
            &mut parts,
            "mail.example.org",
            IpAddr::from([1, 2, 3, 4]),
            &Identity::MailFrom("amy@example.com".into()),
        );
        assert_eq!(
            parts.join(" "),
            "(mail.example.org: domain of amy@example.com has authorized host 1.2.3.4)",
        );
    }

    #[test]
    fn format_none_parts_with_unusual_sender() {
        let mut parts = Vec::new();
        format_none_parts(
            &mut parts,
            "mail.example.org",
            &Identity::MailFrom("\"what(!)\"@example.com".into()),
        );
        assert_eq!(
            parts.join(" "),
            "(mail.example.org: no authorization information available for sender \"what\\(!\\)\"@example.com)",
        );
    }
}
