use std::{borrow::Cow, convert::TryFrom, fmt::Write};

// Implementation note: This module contains code for processing header content.
// The items in this module are from various RFCs. They are required for
// producing compliant `Received-SPF` and `Authentication-Results` headers, and
// for the latter, parsing incoming headers.

// RFC 2045, section 5.1

// value := token / quoted-string
pub fn skip_mime_value(input: &str) -> Option<&str> {
    skip_token(input).or_else(|| skip_quoted_string(input))
}

fn is_token(s: &str) -> bool {
    matches!(skip_token(s), Some(s) if s.is_empty())
}

// token := 1*<any (US-ASCII) CHAR except SPACE, CTLs, or tspecials>
fn skip_token(input: &str) -> Option<&str> {
    fn is_token(c: char) -> bool {
        c.is_ascii_graphic()
            && !matches!(
                c,
                '(' | ')' | '<' | '>' | '@' | ',' | ';' | ':' | '\\' | '"' | '/' | '[' | ']' | '?' | '='
            )
    }

    let mut s = input.strip_prefix(is_token)?;
    while let Some(snext) = s.strip_prefix(is_token) {
        s = snext;
    }
    Some(s)
}

// RFC 5322, section 3.2
// RFC 6532

// CFWS = (1*([FWS] comment) [FWS]) / FWS
pub fn skip_cfws(input: &str) -> Option<&str> {
    enum State { Init, Fws, Comment }

    let mut s = input;
    let mut state = State::Init;

    loop {
        match state {
            State::Init => {
                if let Some(snext) = skip_fws(s) {
                    s = snext;
                    state = State::Fws;
                } else if let Some(snext) = skip_comment(s) {
                    s = snext;
                    state = State::Comment;
                } else {
                    return None;
                }
            }
            State::Fws => {
                if let Some(snext) = skip_comment(s) {
                    s = snext;
                    state = State::Comment;
                } else {
                    break;
                }
            }
            State::Comment => {
                if let Some(snext) = skip_fws(s) {
                    s = snext;
                    state = State::Fws;
                } else if let Some(snext) = skip_comment(s) {
                    s = snext;
                } else {
                    break;
                }
            }
        }
    }

    Some(s)
}

// FWS = ([*WSP CRLF] 1*WSP)
fn skip_fws(input: &str) -> Option<&str> {
    enum State { Init, Wsp, Crlf, CrlfWsp }

    let mut s = input;
    let mut state = State::Init;

    loop {
        match state {
            State::Init => {
                if let Some(snext) = s.strip_prefix(is_wsp) {
                    s = snext;
                    state = State::Wsp;
                } else if let Some(snext) = s.strip_prefix(is_crlf) {
                    s = snext;
                    state = State::Crlf;
                } else {
                    return None;
                }
            }
            State::Wsp => {
                if let Some(snext) = s.strip_prefix(is_wsp) {
                    s = snext;
                } else if let Some(snext) = s.strip_prefix(is_crlf) {
                    s = snext;
                    state = State::Crlf;
                } else {
                    break;
                }
            }
            State::Crlf => {
                if let Some(snext) = s.strip_prefix(is_wsp) {
                    s = snext;
                    state = State::CrlfWsp;
                } else {
                    return None;
                }
            }
            State::CrlfWsp => {
                if let Some(snext) = s.strip_prefix(is_wsp) {
                    s = snext;
                } else {
                    break;
                }
            }
        }
    }

    Some(s)
}

// comment = "(" *([FWS] ccontent) [FWS] ")"
fn skip_comment(input: &str) -> Option<&str> {
    // Note: Implementation is the same as `skip_quoted_string` below.

    enum State { Init, Content, Fws }

    let mut s = input;
    let mut state = State::Init;

    loop {
        match state {
            State::Init => {
                if let Some(snext) = s.strip_prefix('(') {
                    s = snext;
                    state = State::Content;
                } else {
                    return None;
                }
            }
            State::Content => {
                if let Some(snext) = s.strip_prefix(')') {
                    s = snext;
                    break;
                } else if let Some(snext) = skip_ccontent(s) {
                    s = snext;
                } else if let Some(snext) = skip_fws(s) {
                    s = snext;
                    state = State::Fws;
                } else {
                    return None;
                }
            }
            State::Fws => {
                if let Some(snext) = s.strip_prefix(')') {
                    s = snext;
                    break;
                } else if let Some(snext) = skip_ccontent(s) {
                    s = snext;
                    state = State::Content;
                } else {
                    return None;
                }
            }
        }
    }

    Some(s)
}

// ccontent = ctext / quoted-pair / comment
fn skip_ccontent(input: &str) -> Option<&str> {
    skip_ctext(input)
        .or_else(|| skip_quoted_pair(input))
        .or_else(|| skip_comment(input))
}

fn skip_ctext(input: &str) -> Option<&str> {
    input.strip_prefix(is_ctext)
}

fn is_ctext(c: char) -> bool {
    c.is_ascii_graphic() && !matches!(c, '(' | ')' | '\\') || !c.is_ascii()
}

pub fn is_dot_atom(s: &str) -> bool {
    matches!(skip_dot_atom(s), Some(s) if s.is_empty())
}

// dot-atom = 1*atext *("." 1*atext)
// (Actually, `dot-atom-text`, ie `dot-atom` not including surrounding CFWS.)
fn skip_dot_atom(input: &str) -> Option<&str> {
    enum State { Init, Atext }

    let mut s = input;
    let mut state = State::Init;

    loop {
        match state {
            State::Init => {
                if let Some(snext) = s.strip_prefix(is_atext) {
                    s = snext;
                    state = State::Atext;
                } else {
                    return None;
                }
            }
            State::Atext => {
                if let Some(snext) = s
                    .strip_prefix(is_atext)
                    .or_else(|| s.strip_prefix('.').and_then(|s| s.strip_prefix(is_atext)))
                {
                    s = snext;
                } else {
                    break;
                }
            }
        }
    }

    Some(s)
}

fn is_atext(c: char) -> bool {
    c.is_ascii_alphanumeric()
        || matches!(
            c,
            '!' | '#' | '$' | '%' | '&' | '\'' | '*' | '+' | '-' | '/' | '=' | '?' | '^' | '_' | '`'
                | '{' | '|' | '}' | '~'
        )
        || !c.is_ascii()
}

pub fn is_quoted_string(s: &str) -> bool {
    matches!(skip_quoted_string(s), Some(s) if s.is_empty())
}

// quoted-string = DQUOTE *([FWS] qcontent) [FWS] DQUOTE
// (Given our usage contexts, not including the surrounding CFWS, though.)
fn skip_quoted_string(input: &str) -> Option<&str> {
    // Note: Implementation is the same as `skip_comment` above.

    enum State { Init, Content, Fws }

    let mut s = input;
    let mut state = State::Init;

    loop {
        match state {
            State::Init => {
                if let Some(snext) = s.strip_prefix('"') {
                    s = snext;
                    state = State::Content;
                } else {
                    return None;
                }
            }
            State::Content => {
                if let Some(snext) = s.strip_prefix('"') {
                    s = snext;
                    break;
                } else if let Some(snext) = skip_qcontent(s) {
                    s = snext;
                } else if let Some(snext) = skip_fws(s) {
                    s = snext;
                    state = State::Fws;
                } else {
                    return None;
                }
            }
            State::Fws => {
                if let Some(snext) = s.strip_prefix('"') {
                    s = snext;
                    break;
                } else if let Some(snext) = skip_qcontent(s) {
                    s = snext;
                    state = State::Content;
                } else {
                    return None;
                }
            }
        }
    }

    Some(s)
}

// qcontent = qtext / quoted-pair
fn skip_qcontent(input: &str) -> Option<&str> {
    input.strip_prefix(is_qtext).or_else(|| skip_quoted_pair(input))
}

fn is_qtext(c: char) -> bool {
    c.is_ascii_graphic() && !matches!(c, '"' | '\\') || !c.is_ascii()
}

// quoted-pair = ("\" (VCHAR / WSP))
fn skip_quoted_pair(input: &str) -> Option<&str> {
    input
        .strip_prefix('\\')
        .and_then(|s| s.strip_prefix(|c| is_vchar(c) || is_wsp(c)))
}

// RFC 5234, appendix B.1

fn is_wsp(c: char) -> bool {
    matches!(c, ' ' | '\t')
}

fn is_crlf(c: char) -> bool {
    // In the milter library, line breaks in the header field value are
    // represented as LF, not CRLF.
    c == '\n'
}

fn is_vchar(c: char) -> bool {
    c.is_ascii_graphic() || !c.is_ascii()
}

/// Escapes arbitrary inputs for use as a single (not folded) word in an RFC
/// 5322 comment. Whitespace or nested comments are not recognised but escaped
/// as literal.
pub fn escape_comment_word(s: &str) -> Cow<'_, str> {
    if s.chars().all(is_ctext) {
        s.into()
    } else {
        let mut result = String::with_capacity(s.len());
        for c in s.chars() {
            if is_ctext(c) {
                result.push(c);
            } else if is_wsp(c) || matches!(c, '(' | ')' | '\\') {
                result.push('\\');
                result.push(c);
            } else {
                write_escaped_ascii_char(&mut result, c);
            }
        }
        result.into()
    }
}

/// Encodes the given string as a key-value-pair value, as per RFC 7208, section
/// 9.1.
pub fn encode_value(s: &str) -> Cow<'_, str> {
    if is_dot_atom(s) {
        s.into()
    } else {
        encode_quoted_string(s).into()
    }
}

/// Encodes the given string as an RFC 2045 `value`.
pub fn encode_mime_value(s: &str) -> Cow<'_, str> {
    if is_token(s) {
        s.into()
    } else {
        encode_quoted_string(s).into()
    }
}

/// Encodes the given string as an RFC 5322 `quoted-string`. This produces one
/// unbroken quoted string as a unit, without recognising or introducing folding
/// whitespace.
fn encode_quoted_string(s: &str) -> String {
    let mut result = String::with_capacity(s.len() + 2);
    result.push('"');
    for c in s.chars() {
        if is_qtext(c) || is_wsp(c) {
            result.push(c);
        } else if matches!(c, '"' | '\\') {
            result.push('\\');
            result.push(c);
        } else {
            write_escaped_ascii_char(&mut result, c);
        }
    }
    result.push('"');
    result
}

fn write_escaped_ascii_char(s: &mut String, c: char) {
    let c = u8::try_from(c as u32).expect("character not in ASCII range");

    // This escape sequence format does not have a formal interpretation, we
    // choose it to be helpful in debugging.
    write!(s, "\\\\x{:02x}", c).unwrap();
}

/// Decodes the given quoted string and returns its (semantic) content. The
/// input must be a valid quoted string.
pub fn decode_quoted_string(s: &str) -> String {
    assert!(is_quoted_string(s));

    // Strip surrounding double quotes.
    let s = &s[1..(s.len() - 1)];

    let mut result = String::with_capacity(s.len());

    // Copy the string into the result, but
    // - remove CRLF (which is always part of FWS); and
    // - replace quoted-pair with the quoted character.
    let mut escape = false;
    for c in s.chars() {
        if escape {
            escape = false;
            result.push(c);
        } else if c == '\\' {
            escape = true;
        } else if !is_crlf(c) {
            result.push(c);
        }
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn skip_mime_value_ok() {
        assert_eq!(skip_mime_value("abc"), Some(""));
        assert_eq!(skip_mime_value("abc<"), Some("<"));
        assert_eq!(skip_mime_value(".abc."), Some(""));
        assert_eq!(skip_mime_value("-_-"), Some(""));
        assert_eq!(skip_mime_value("🎈"), None);
        assert_eq!(skip_mime_value("\"🎈\""), Some(""));
        assert_eq!(skip_mime_value("\"\\\" <\"\\"), Some("\\"));
    }

    #[test]
    fn skip_fws_ok() {
        assert_eq!(skip_fws("x"), None);
        assert_eq!(skip_fws("\t"), Some(""));
        assert_eq!(skip_fws("\t\tx"), Some("x"));
        assert_eq!(skip_fws("\n"), None);
        assert_eq!(skip_fws("\nx"), None);
        assert_eq!(skip_fws("\n\t"), Some(""));
        assert_eq!(skip_fws("\n\tx"), Some("x"));
        assert_eq!(skip_fws("\n\t\t"), Some(""));
        assert_eq!(skip_fws("\n\t\tx"), Some("x"));
        assert_eq!(skip_fws("\t\n"), None);
        assert_eq!(skip_fws("\t\nx"), None);
        assert_eq!(skip_fws("\t\t\n\t\tx"), Some("x"));
    }

    #[test]
    fn skip_comment_ok() {
        assert_eq!(skip_comment("xy"), None);
        assert_eq!(skip_comment("(abc) x"), Some(" x"));
        assert_eq!(skip_comment("(ab\nc) x"), None);
        assert_eq!(skip_comment("(ab\n c) x"), Some(" x"));
        assert_eq!(skip_comment("(ab\n \\\\) x"), Some(" x"));
        assert_eq!(skip_comment("(ab\n \\ ) x"), Some(" x"));
        assert_eq!(skip_comment("(merhaba dünya) x"), Some(" x"));
        assert_eq!(skip_comment("(x \n\t(\n\tx)(ab)())x"), Some("x"));
    }

    #[test]
    fn skip_ctext_ok() {
        assert_eq!(skip_ctext("x."), Some("."));
        assert_eq!(skip_ctext("ß."), Some("."));
        assert_eq!(skip_ctext("?."), Some("."));
        assert_eq!(skip_ctext("(."), None);
        assert_eq!(skip_ctext("\t."), None);
    }

    #[test]
    fn is_dot_atom_ok() {
        assert!(!is_dot_atom(""));
        assert!(is_dot_atom("x"));
        assert!(is_dot_atom("𝔵"));

        assert!(!is_dot_atom("."));
        assert!(!is_dot_atom(".."));
        assert!(!is_dot_atom("x."));
        assert!(!is_dot_atom(".y"));
        assert!(is_dot_atom("x.y"));
        assert!(!is_dot_atom(".x.y"));
        assert!(!is_dot_atom("x.y."));

        assert!(is_dot_atom("mail.example.org"));
        assert!(is_dot_atom("mail.例子.org"));
        assert!(!is_dot_atom("mx:mail.example.org"));
    }

    #[test]
    fn skip_quoted_pair_ok() {
        assert_eq!(skip_quoted_pair("x"), None);
        assert_eq!(skip_quoted_pair("\\"), None);
        assert_eq!(skip_quoted_pair("\\x"), Some(""));
        assert_eq!(skip_quoted_pair("\\𝔵"), Some(""));
        assert_eq!(skip_quoted_pair("\\xy"), Some("y"));
        assert_eq!(skip_quoted_pair("\\\\"), Some(""));
        assert_eq!(skip_quoted_pair("\\\\y"), Some("y"));
        assert_eq!(skip_quoted_pair("\\ x"), Some("x"));
        assert_eq!(skip_quoted_pair("\\(x"), Some("x"));
        assert_eq!(skip_quoted_pair("\\\n"), None);
    }

    #[test]
    fn escape_comment_word_ok() {
        assert_eq!(escape_comment_word("ab c"), "ab\\ c");
        assert_eq!(escape_comment_word("ab)c"), "ab\\)c");
        assert_eq!(escape_comment_word("ab\"c"), "ab\"c");
        assert_eq!(escape_comment_word("a我c"), "a我c");
        assert_eq!(escape_comment_word("a\nc"), "a\\\\x0ac");
        assert_eq!(escape_comment_word("a\x7fc"), "a\\\\x7fc");
        assert_eq!(escape_comment_word("\"ab  c(xy\\"), "\"ab\\ \\ c\\(xy\\\\");
    }

    #[test]
    fn encode_mime_value_ok() {
        assert_eq!(encode_mime_value("my.host"), "my.host");
        assert_eq!(encode_mime_value("my.host/1244"), "\"my.host/1244\"");
        assert_eq!(encode_mime_value("timed out"), "\"timed out\"");
    }

    #[test]
    fn decode_quoted_string_ok() {
        assert_eq!(decode_quoted_string("\"a bc\\q🚀 \n  \\我\\\"\""), "a bcq🚀   我\"");
    }
}
