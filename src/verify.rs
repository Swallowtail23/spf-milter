use crate::{
    config::{model::ExpModification, Config},
    resolver::{DomainResolver, MockResolver},
};
use std::{
    borrow::Cow,
    fmt::{self, Display, Formatter},
    net::IpAddr,
};
use viaspf::{Name, QueryResult, SpfResult, SpfResultCause};

/// An SPF HELO or MAIL FROM identity.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum Identity {
    Helo(String),
    MailFrom(String),
}

impl Identity {
    pub fn name(&self) -> &'static str {
        match self {
            Self::Helo(_) => "helo",
            Self::MailFrom(_) => "mailfrom",
        }
    }
}

impl AsRef<str> for Identity {
    fn as_ref(&self) -> &str {
        match self {
            Self::Helo(s) | Self::MailFrom(s) => s,
        }
    }
}

impl Display for Identity {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.as_ref().fmt(f)
    }
}

/// A verification result produced by verifying an identity.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct VerificationResult {
    pub identity: Identity,
    pub spf_result: SpfResult,
    pub cause: Option<SpfResultCause>,
}

impl VerificationResult {
    pub fn new(identity: Identity, result: QueryResult) -> Self {
        Self {
            identity,
            spf_result: result.result,
            cause: result.cause,
        }
    }
}

// Design note: The asymmetry in the `verify_*` functions is deliberate. At the
// end of the authorisation process we want to have a final result. Therefore,
// the MAIL FROM identity always produces a result, even with unusable inputs.
// The HELO identity, on the other hand, may legitimately be an address literal
// or may be missing entirely; verification is not done in such cases.

pub fn verify_helo_identity(
    config: &Config,
    mock_resolver: Option<&MockResolver>,
    hostname: &str,
    ip: IpAddr,
    helo_host: &str,
) -> Option<VerificationResult> {
    let config = to_verifier_config(config, hostname);

    // The HELO hostname is not necessarily in FQDN format (perhaps even a
    // mailbox like ‘x@y.org’ – acceptable to the viaspf library!). The spec
    // says that a HELO check can only be performed when HELO is an FQDN, so we
    // make verification dependent on this condition.
    if is_fqdn(helo_host) {
        let result = verify(&config, mock_resolver, ip, helo_host, helo_host);
        Some(VerificationResult::new(Identity::Helo(helo_host.into()), result))
    } else {
        None
    }
}

pub fn prepare_mail_from_identity<'a>(mut mail_from: &'a str, helo_host: &str) -> Cow<'a, str> {
    if let Some(s) = mail_from.strip_prefix('<').and_then(|s| s.strip_suffix('>')) {
        mail_from = s;
    }

    // Section 2.4 of RFC 7208 mandates that the HELO identity with local-part
    // `postmaster` be used when the MAIL FROM identity is empty. (The resulting
    // mailbox may be ill-formed.)
    if mail_from.is_empty() {
        format!("postmaster@{}", helo_host).into()
    } else {
        mail_from.into()
    }
}

pub fn verify_mail_from_identity(
    config: &Config,
    mock_resolver: Option<&MockResolver>,
    hostname: &str,
    ip: IpAddr,
    mail_from: &str,
    helo_host: &str,
) -> VerificationResult {
    let config = to_verifier_config(config, hostname);

    let result = verify(&config, mock_resolver, ip, mail_from, helo_host);

    VerificationResult::new(Identity::MailFrom(mail_from.into()), result)
}

fn verify(
    config: &viaspf::Config,
    mock_resolver: Option<&MockResolver>,
    ip: IpAddr,
    sender: &str,
    helo_host: &str,
) -> QueryResult {
    // Only here is the optional mock resolver used to bypass instantiation of
    // the ‘live’ DNS resolver.
    match mock_resolver {
        Some(mock) => viaspf::evaluate_spf(mock, config, ip, sender, helo_host),
        None => {
            let resolver = DomainResolver::new(config.timeout());
            viaspf::evaluate_spf(&resolver, config, ip, sender, helo_host)
        }
    }
}

pub fn is_fqdn(s: &str) -> bool {
    Name::domain(s).is_ok()
}

fn to_verifier_config(config: &Config, hostname: &str) -> viaspf::Config {
    let mut builder = viaspf::Config::builder()
        .hostname(hostname)
        .timeout(config.timeout())
        .default_explanation(config.fail_reply_text().to_owned())
        .max_void_lookups(config.max_void_lookups());

    match config.fail_reply_text_exp() {
        ExpModification::Substitute(explain_string) => {
            let explain_string = explain_string.to_owned();
            builder = builder.modify_exp_with(move |exp| {
                exp.segments = explain_string.segments.clone();
            });
        }
        ExpModification::Decorate { prefix, suffix } => {
            let (prefix, suffix) = (prefix.to_owned(), suffix.to_owned());
            builder = builder.modify_exp_with(move |exp| {
                exp.segments.splice(..0, prefix.segments.iter().cloned());
                exp.segments.extend(suffix.segments.iter().cloned());
            });
        }
    }

    builder.build()
}

#[cfg(test)]
mod tests {
    use super::*;
    use viaspf::record::ExplainString;

    #[test]
    fn to_verifier_config_exp_mod_decorate() {
        let config = Config::builder("unused")
            .fail_reply_text_exp(ExpModification::Decorate {
                prefix: "<".parse().unwrap(),
                suffix: ">".parse().unwrap(),
            })
            .build()
            .unwrap();

        let config = to_verifier_config(&config, "unused");

        let f = config.modify_exp_fn().unwrap();

        let mut explain_string = "denied".parse().unwrap();
        f(&mut explain_string);

        // Have to compare string representations, because the decorated and the
        // parsed explain string have different internal structure!
        assert_eq!(
            explain_string.to_string(),
            "<denied>".parse::<ExplainString>().unwrap().to_string()
        );
    }
}
