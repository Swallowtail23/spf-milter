use crate::{
    auth::AuthSession, config, header::auth_results::AuthenticationResultsHeader, resolver,
};
use log::debug;
use milter::{Actions, DataHandle, MacroValue, ProtocolOpts, Stage, Status};
use std::net::SocketAddr;

/// A trait providing mutable access to the current authorisation session.
trait SessionMut {
    fn session(&mut self) -> &mut AuthSession;
}

impl SessionMut for DataHandle<AuthSession> {
    fn session(&mut self) -> &mut AuthSession {
        self.borrow_mut().expect("milter context data not available")
    }
}

type Context = milter::Context<AuthSession>;

#[milter::on_negotiate(negotiate_callback)]
fn handle_negotiate(
    mut context: Context,
    actions: Actions,
    _: ProtocolOpts,
) -> milter::Result<(Status, Actions, ProtocolOpts)> {
    // Reload the configuration at the very beginning, then keep it unchanged
    // for the rest of the session by storing it in the context. This is
    // necessary because we make actions/opts below dependent on configuration.
    config::reload_if_stale();

    let config = config::current();

    let mut required_actions = Actions::empty();
    if !config.dry_run() {
        required_actions |= Actions::ADD_HEADER;
        if config.delete_incoming_authentication_results() {
            required_actions |= Actions::REPLACE_HEADER;
        }
    }

    assert!(actions.contains(required_actions), "required milter actions not supported");

    // Request that no macros be sent on this connection except the ones that we
    // might actually look at.
    context.api.request_macros(
        Stage::Connect,
        if config.hostname().is_none() { "j" } else { "" },
    )?;
    context.api.request_macros(Stage::Helo, "")?;
    context.api.request_macros(
        Stage::Mail,
        if config.trust_authenticated_senders() { "{auth_authen}" } else { "" },
    )?;
    context.api.request_macros(Stage::Rcpt, "")?;
    context.api.request_macros(Stage::Data, "i")?;
    context.api.request_macros(Stage::Eoh, "")?;
    context.api.request_macros(Stage::Eom, "")?;

    let session = AuthSession::new(config);

    context.data.replace(session)?;

    Ok((Status::Continue, required_actions, Default::default()))
}

#[milter::on_connect(connect_callback)]
fn handle_connect(
    mut context: Context,
    _: &str,
    socket_addr: Option<SocketAddr>,
) -> milter::Result<Status> {
    let ip = match socket_addr {
        Some(addr) => addr.ip(),
        None => {
            debug!("accepted connection with no IP address available");
            return Ok(Status::Accept);
        }
    };

    let session = context.data.session();

    if session.config.trusted_networks().contains(ip) {
        if ip.is_loopback() {
            debug!("accepted local connection");
        } else {
            debug!("accepted connection from trusted network address {}", ip);
        }
        return Ok(Status::Accept);
    }

    let hostname = match session.config.hostname() {
        Some(hostname) => hostname,
        None => context.api.macro_value("j")?.expect("requested macro not available"),
    };

    let hostname = hostname.to_owned();
    session.init_connection(hostname, ip);

    Ok(Status::Continue)
}

#[milter::on_helo(helo_callback)]
fn handle_helo(mut context: Context, helo_host: &str) -> milter::Result<Status> {
    let session = context.data.session();

    let mock_resolver = resolver::get_mock();

    session.authorize_helo(&context.api, helo_host, mock_resolver)
}

#[milter::on_mail(mail_callback)]
fn handle_mail(mut context: Context, smtp_args: Vec<&str>) -> milter::Result<Status> {
    let session = context.data.session();

    if session.config.trust_authenticated_senders() {
        if let Some(login) = context.api.macro_value("{auth_authen}")? {
            debug!("accepted message from sender authenticated as \"{}\"", login);
            return Ok(Status::Accept);
        }
    }

    let mail_from = smtp_args[0];
    let mock_resolver = resolver::get_mock();

    session.authorize_mail_from(&context.api, mail_from, mock_resolver)
}

#[milter::on_header(header_callback)]
fn handle_header(mut context: Context, name: &str, value: &str) -> milter::Result<Status> {
    if name.eq_ignore_ascii_case(AuthenticationResultsHeader::NAME) {
        let session = context.data.session();

        let id = queue_id(&context.api)?;

        session.process_auth_results_header(id, value);
    }

    Ok(Status::Continue)
}

#[milter::on_eom(eom_callback)]
fn handle_eom(mut context: Context) -> milter::Result<Status> {
    let session = context.data.session();

    let id = queue_id(&context.api)?;

    session.finish_message(&context.api, id)
}

#[milter::on_abort(abort_callback)]
fn handle_abort(mut context: Context) -> Status {
    let session = context.data.session();

    session.abort_message();

    Status::Continue
}

#[milter::on_close(close_callback)]
fn handle_close(mut context: Context) -> milter::Result<Status> {
    context.data.take()?;

    Ok(Status::Continue)
}

fn queue_id(macros: &impl MacroValue) -> milter::Result<&str> {
    macros.macro_value("i").map(|id| id.unwrap_or("NONE"))
}
