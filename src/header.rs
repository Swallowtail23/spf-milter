pub mod auth_results;
pub mod format;
pub mod received_spf;

use std::fmt::{self, Display, Formatter};

/// A trait for entities that can produce a `HeaderField`.
pub trait ToHeaderField {
    /// Creates a `HeaderField`. This may be a costly operation, as the
    /// constituent parts of the header field body are eagerly encoded and
    /// aggregated.
    fn to_header_field(&self) -> HeaderField;
}

// See RFC 5322, 2.1.1.
const HEADER_LINE_WIDTH: usize = 78;

/// A header field consisting of a constant field name and a preprocessed field
/// body (the header ‘value’) ready for formatting into a message header.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct HeaderField {
    name: FieldName,
    body: FieldBody,
}

impl HeaderField {
    pub fn name(&self) -> &'static str {
        self.name.0
    }

    /// Returns a formatted representation of the header field body, with lines
    /// wrapped according to RFC 5322 recommendations.
    pub fn format_body(&self) -> String {
        format_header_value(
            self.name.0.len() + 1,  // name length plus colon
            HEADER_LINE_WIDTH,
            &self.body.0,
        )
    }
}

impl Display for HeaderField {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}: {}", self.name.0, self.body.0.join(" "))
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
struct FieldName(&'static str);

// The wrapped vector contains pieces of the field body that are properly
// encoded, with escaping or quoting already applied. The pieces should tolerate
// having FWS inserted between them; the purpose of having the pieces is simply
// *formatting*, ie wrapping the field body over multiple lines.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
struct FieldBody(Vec<String>);

fn format_header_value<I, S>(initlen: usize, limit: usize, items: I) -> String
where
    I: IntoIterator<Item = S>,
    S: AsRef<str>,
{
    // Formatting is based on Unicode character count, not octet length. See RFC
    // 6532, section 3.4.
    let mut i = initlen;
    let mut value = String::new();

    for (n, item) in items.into_iter().enumerate() {
        let item = item.as_ref();
        let len = item.chars().count() + 1;  // item length plus leading space or tab
        if i + len <= limit {
            // Skip initial space, as the space after the field name is
            // automatically added by the milter library.
            if n != 0 {
                value.push(' ');
            }
        } else {
            // Never add `\n\t` at the very beginning of the field value.
            if n != 0 {
                value.push_str("\n\t");
                i = 0;
            }
        };
        value.push_str(item);
        i += len;
    }

    value
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn format_header_value_ok() {
        let parts = vec!["one", "two", "three", "four", "five"];
        assert_eq!(
            format_header_value(0, 10, &parts),
            "one two\n\
            \tthree\n\
            \tfour five",
        );
        assert_eq!(
            format_header_value(0, 11, &parts),
            "one two\n\
            \tthree four\n\
            \tfive",
        );
        assert_eq!(
            format_header_value(7, 11, &parts),
            "one\n\
            \ttwo three\n\
            \tfour five",
        );
        assert_eq!(
            format_header_value(8, 11, &parts),
            "one\n\
            \ttwo three\n\
            \tfour five",
        );

        let parts = vec!["一二三", "two", "three", "four", "five"];
        assert_eq!(
            format_header_value(0, 10, &parts),
            "一二三 two\n\
            \tthree\n\
            \tfour five",
        );
    }
}
