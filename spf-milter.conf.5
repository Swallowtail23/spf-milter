.TH SPF-MILTER.CONF 5 2021-05-20
.SH NAME
spf-milter.conf \- SPF Milter configuration file
.SH SYNOPSIS
.B /etc/spf-milter.conf
.SH DESCRIPTION
.I spf-milter.conf
is the configuration file for SPF Milter.
.BR spf-milter (8)
is a milter application that verifies email senders using the
.I Sender Policy Framework
protocol.
.PP
An SPF Milter configuration file contains configuration parameters following a
simple key\-value scheme.
The following listing shows the contents of a sample
.I /etc/spf-milter.conf
file.
.PP
.in +4n
.EX
# Milter socket (where the MTA connects)
socket = inet:3000@localhost

# SPF verification
verify_helo = yes
reject_results = fail, permerror

# Host and network
hostname = my.host.org
trusted_networks = loopback, 12.45.2.192/28
.EE
.in
.PP
SPF Milter is an implementation of the SPF protocol specified in RFC 7208.
Terms and concepts are to be interpreted according to RFC 7208.
In particular, the notions of
.I HELO
and
.I MAIL FROM identity
and their processing in sequence are central to SPF Milter operation.
Verification of sender identities involves querying the DNS to retrieve the SPF
policy, and evaluate it using the identity and IP address to obtain a final
.IR "SPF result" .
Depending on the SPF result, messages may be refused at the SMTP level or have
the result recorded in the message header.
.PP
The configuration parameters available to configure the various facets of SPF
processing are described fully in section
.BR PARAMETERS .
Whether or not to verify the HELO identity can be controlled with the
.B verify_helo
parameter.
When HELO verification is enabled, the decision of whether to verify the MAIL
FROM identity is conditional on the HELO verification result.
Once an SPF result has been determined, a message may be rejected or accepted.
The parameters
.B reject_results
and
.B reject_helo_results
control which results are to be rejected with an SMTP error reply.
By default, this set contains the negative authorization result
.I fail
and the error results
.I temperror
and
.IR permerror .
Results that are not rejected will instead be recorded in the message header.
The parameter
.B header
selects the type of header field to generate.
.PP
The configuration file format uses a flat key\-value scheme with one parameter
per line.
Key and value are separated with the character
.BR = .
Whitespace before and after key and value is not significant.
Blank lines and lines whose first non-blank character is
.B #
are ignored.
Some parameters accept a set of fixed values: these should be written as
comma-separated values (abbreviated
.I CSV
below), where whitespace before and after a comma is not significant.
For all parameters, the possible values are described below.
As a rule, case is significant.
Boolean values are
.B yes
and
.B no
(alias
.B true
and
.BR false ).
For a few parameters corresponding command-line options exist.
Command-line options take precedence over parameters set in the configuration
file.
.PP
Configuration can be reloaded during operation by sending the signal
.B SIGUSR1
to the milter process.
Reloading is asynchronous: the reload from disk occurs when the next connection
to the milter is opened.
If reloading fails, an error is logged and the in-memory configuration remains
active.
Currently open connections continue to work with their initial configuration, in
order to preserve configuration integrity over the lifetime of a connection.
A small set of parameters configured at program start cannot be reloaded: the
parameters
.BR socket ,
.BR log_destination ,
.BR log_level ,
and
.BR syslog_facility .
Options passed on the command-line are also not reloaded.
.SH PARAMETERS
.TP
.BR authserv_id " (\fIstring\fR)"
The
.I authserv-id
or
.I authentication service identifier
to use in
.I Authentication-Results
header fields.
Refer to RFC 8601, section 2.5, for a description of this element.
Only the unquoted syntax is supported for the
.I authserv-id
value.
Defaults to the
.B hostname
setting.
.TP
.BR definitive_helo_results " (\fICSV\fR: " pass ", " fail ", " softfail ", " temperror ", " permerror )
The set of HELO verification results to treat as definitive.
When HELO identity verification produces one of the SPF results in this set, the
outcome is considered conclusive, and verification of the MAIL FROM identity is
subsequently not done.
The default is the empty set.
.TP
.BR delete_incoming_authentication_results " (\fIboolean\fR)"
Whether to delete incoming
.I Authentication-Results
header fields whose
.I authserv-id
matches the
.B authserv_id
setting.
Header deletion is done at the end of the authorization process, before
insertion of the
.I Received-SPF
or
.I Authentication-Results
header field.
Caution: If there are multiple milters editing the same headers, there is the
possibility of interference.
When enabling this setting, it is not recommended to run other milters that may
also perform deletion of the
.I Authentication-Results
header field.
Defaults to
.B yes
if
.B header
contains
.BR Authentication-Results ,
.B no
otherwise.
.TP
.BR dry_run " (\fIboolean\fR)"
Whether to take action or apply modifications during processing.
When this setting is enabled, SPF verification is performed on senders, but
authorization is not enforced: no senders are rejected at the SMTP level, and no
headers are added to or deleted from messages.
The default is
.BR no .
.TP
.BR fail_reply_code " (\fB4\fR\fIXX\fR | \fB5\fR\fIXX\fR)"
The SMTP reply code to use when rejecting senders due to a
.I fail
result.
The reply code must agree in the first digit with the
.B fail_status_code
setting.
The default is
.BR 550 .
.TP
.BR fail_reply_text " (\fImacro-string\fR)"
The SMTP reply text to use when rejecting senders due to a
.I fail
result.
The text must be a valid
.I explain-string
per RFC 7208.
This value will undergo macro expansion, where macros are expanded according to
the rules in RFC 7208, section 7.2.
Note that this parameter is unlike the other reply text configuration
parameters, which are verbatim strings.
The default is “SPF validation failed”.
.TP
.BR fail_reply_text_exp " (\fImacro-string\fR)"
The SMTP reply text to use when rejecting senders due to a
.I fail
result, when the explanation string was provided by a third party via an
.I exp
modifier.
As with
.BR fail_reply_text ,
the text must be a valid
.I explain-string
per RFC 7208, with the additional
feature that it may contain one occurrence of the special token
.BR %{exp} .
The
.B %{exp}
token is replaced with the explanation obtained from the DNS during macro
expansion.
The default is “SPF validation failed: %{o} explains: %{exp}”.
.TP
.BR fail_status_code " (\fB4.\fR\fIX\fR\fB.\fR\fIX\fR | \fB5.\fR\fIX\fR\fB.\fR\fIX\fR)
The enhanced status code to use when rejecting senders due to a
.I fail
result.
The enhanced status code must agree in the first digit with the
.B fail_reply_code
setting.
The default is
.BR 5.7.23 .
.TP
.BR header " (\fICSV\fR: " Received-SPF ", " Authentication-Results )
The header field type to use when adding a header to a message.
The
.I Received-SPF
header field is specified in RFC 7208.
The
.I Authentication-Results
header field is specified in RFC 8601.
This parameter may also be set to the empty value, to suppress addition of a
header, or to a comma-separated ordered list of both header field types, to add
both headers to a message.
The default is
.BR Received-SPF .
.TP
.BR hostname " (\fIstring\fR)"
This host’s domain name.
The hostname is used in the generated header field (in an
.I Authentication-Results
header only if not overridden with the
.B authserv_id
parameter), and substituted for the
.B %{r}
macro in macro-strings.
Defaults to the MTA’s hostname (the value of the sendmail macro
.BR j ).
.TP
.BR include_all_results " (\fIboolean\fR)"
Whether to include all available verification results in generated headers.
When this setting is enabled, the verification results for both the HELO and
MAIL FROM identity are recorded in the message header instead of just the
definitive result.
The default is
.BR no .
.TP
.BR include_mailfrom_local_part " (\fIboolean\fR)"
Whether to include the
.I local-part
of MAIL FROM identities in the generated
.I Authentication-Results
header.
Use of this parameter is discouraged per RFC 8601, section 2.7.2.
It is provided for compatibility only.
The default is
.BR no .
.TP
.BR log_destination " (" syslog " | " stderr )
The destination for log messages.
Log destination
.B syslog
causes log messages to be written to the system logger.
Log destination
.B stderr
causes log messages to be written to standard error.
This setting cannot be reloaded.
The default is
.BR syslog .
.TP
.BR log_level " (" error " | " warn " | " info " | " debug )
The minimum severity level of messages to log.
Log level
.B error
only logs conditions where immediate administrator intervention is needed.
Log level
.B warn
also logs conditions where administrator attention is recommended.
Log level
.B info
also logs the SPF result for each verified identity; these messages are purely
informational.
Finally, log level
.B debug
also logs detailed messages that provide insight into SPF Milter operation.
This setting cannot be reloaded.
The default is
.BR info .
.TP
.BR max_void_lookups " (\fIinteger\fR)"
The maximum number of void lookups to allow in an SPF query.
Refer to RFC 7208, section 4.6.4, for a discussion of the void lookup limit.
The default is
.BR 2 .
.TP
.BR milter_debug_level " (\fIinteger\fR)"
The trace debug level of the milter library.
The debug level should be an integer value between
.B 0
and
.BR 6 .
The higher the debug level, the more details are included in the trace.
Trace log messages are written to syslog regardless of the
.B log_destination
setting.
The debug level is passed to the milter library as-is.
The default is
.BR 0 .
.TP
.BR permerror_reply_code " (\fB4\fR\fIXX\fR | \fB5\fR\fIXX\fR)"
The SMTP reply code to use when rejecting senders due to a
.I permerror
result.
The reply code must agree in the first digit with the
.B permerror_status_code
setting.
The default is
.BR 550 .
.TP
.BR permerror_reply_text " (\fIstring\fR)"
The SMTP reply text to use when rejecting senders due to a
.I permerror
result.
The default is “SPF validation error”.
.TP
.BR permerror_status_code " (\fB4.\fR\fIX\fR\fB.\fR\fIX\fR | \fB5.\fR\fIX\fR\fB.\fR\fIX\fR)
The enhanced status code to use when rejecting senders due to a
.I permerror
result.
The enhanced status code must agree in the first digit with the
.B permerror_reply_code
setting.
The default is
.BR 5.7.24 .
.TP
.BR reject_helo_results " (\fICSV\fR: " fail ", " softfail ", " temperror ", " permerror )
The set of SPF results to reject at the SMTP level when verifying the HELO
identity.
When this parameter is not used, the
.B reject_results
setting applies to both the MAIL FROM and HELO identity.
Defaults to the
.B reject_results
setting.
.TP
.BR reject_results " (\fICSV\fR: " fail ", " softfail ", " temperror ", " permerror )
The set of SPF results to reject at the SMTP level.
A message from a sender with a final SPF result falling in this set is rejected
with an SMTP error reply.
The SMTP reply code configured for the result determines whether a transient or
permanent error reply is used.
The default is
.RB “ "fail, temperror, permerror" ”.
.TP
.BR skip_senders_file " (\fIpath\fR)"
Path to file with sender identities to skip.
HELO and MAIL FROM identities matching one of the entries in the file at
.I path
will bypass SPF verification.
The file contents must be one entry per line, with entries being domain names
.RB ( example.com )
or email addresses
.RB ( user@example.com ).
Subdomains can be matched by prepending
.B .
to the domain name
.RB ( .example.com ).
Blank lines and lines whose first non-blank character is
.B #
are ignored.
By default no senders are skipped.
.TP
.BR socket " (\fIsocket-spec\fR)"
The listening socket of the milter.
This is a mandatory parameter.
The
.I socket-spec
string is a description of the socket that the milter library should open for
the connection from the MTA.
It can be either an IPv4/IPv6 TCP socket in the form
.BI inet: port @ host
or
.BI inet6: port @ host
(for example,
.BR inet:3000@localhost ),
or a UNIX domain socket in the form
.BI unix: path
(for example,
.BR unix:/run/spf-milter.sock ).
The socket specification is passed to the milter library as-is.
This setting cannot be reloaded.
.TP
.BR softfail_reply_code " (\fB4\fR\fIXX\fR | \fB5\fR\fIXX\fR)"
The SMTP reply code to use when rejecting senders due to a
.I softfail
result.
The reply code must agree in the first digit with the
.B softfail_status_code
setting.
The default is
.BR 550 .
.TP
.BR softfail_reply_text " (\fIstring\fR)"
The SMTP reply text to use when rejecting senders due to a
.I softfail
result.
The default is “SPF validation failed”.
.TP
.BR softfail_status_code " (\fB4.\fR\fIX\fR\fB.\fR\fIX\fR | \fB5.\fR\fIX\fR\fB.\fR\fIX\fR)
The enhanced status code to use when rejecting senders due to a
.I softfail
result.
The enhanced status code must agree in the first digit with the
.B softfail_reply_code
setting.
The default is
.BR 5.7.23 .
.TP
.BR syslog_facility " (\fIfacility\fR)"
The facility to use for syslog messages.
The facility must be one of the named facilities defined by syslog (in lower
case minus the
.B LOG_
prefix).
See
.BR syslog (3).
This setting cannot be reloaded.
The default is
.BR mail .
.TP
.BR temperror_reply_code " (\fB4\fR\fIXX\fR | \fB5\fR\fIXX\fR)"
The SMTP reply code to use when rejecting senders due to a
.I temperror
result.
The reply code must agree in the first digit with the
.B temperror_status_code
setting.
The default is
.BR 451 .
.TP
.BR temperror_reply_text " (\fIstring\fR)"
The SMTP reply text to use when rejecting senders due to a
.I temperror
result.
The default is “SPF validation error”.
.TP
.BR temperror_status_code " (\fB4.\fR\fIX\fR\fB.\fR\fIX\fR | \fB5.\fR\fIX\fR\fB.\fR\fIX\fR)
The enhanced status code to use when rejecting senders due to a
.I temperror
result.
The enhanced status code must agree in the first digit with the
.B temperror_reply_code
setting.
The default is
.BR 4.7.24 .
.TP
.BR timeout_secs " (\fIinteger\fR)"
The timeout duration in seconds for an SPF query.
The timeout duration is the minimum amount of time until the query might abort
due to a timeout.
It is not an exact amount of time, and in the extreme case a query may run for
up to twice this amount of time until it times out.
The default is
.BR 20 .
.TP
.BR trust_authenticated_senders " (\fIboolean\fR)"
Whether to trust authenticated senders.
When this setting is enabled, authenticated senders will bypass SPF
verification.
Note that this setting only allows bypassing verification of the
.I MAIL FROM
identity, not the
.I HELO
identity, as the authentication status is not available at the time when the
HELO identity is verified.
The default is
.BR yes .
.TP
.BR trusted_networks " (\fICSV\fR: \fInetworks\fR)"
Trusted network addresses.
Clients connecting from a trusted network address will bypass SPF verification.
.I networks
must be a comma-separated list of IPv4 or IPv6 addresses and networks (in CIDR
notation).
The shorthand token
.B loopback
can be included to trust connections from any loopback address.
The default is
.BR loopback .
.TP
.BR verify_helo " (\fIboolean\fR)"
Whether to perform SPF verification for the HELO identity.
When this setting is enabled, the HELO identity is verified before the MAIL FROM
identity.
If verification of the HELO identity produces a result in the set
.BR reject_helo_results ,
the sender is rejected with an SMTP error reply; else, if verification produces
a result in the set
.BR definitive_helo_results ,
this result is treated as the final SPF result, and the MAIL FROM identity is
subsequently not verified; else, the verification result is discarded, and the
MAIL FROM identity is instead verified in order to obtain the final SPF result.
A HELO name that is not a fully-qualified domain name is never verified.
When this setting is not enabled, SPF verification is only performed for the
MAIL FROM identity.
The default is
.BR yes .
.SH FILES
.I /etc/spf-milter.conf
.SH SEE ALSO
.BR spf-milter (8)
