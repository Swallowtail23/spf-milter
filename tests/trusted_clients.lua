-- Test 1: Accept connection from loopback address

conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.conninfo(conn, "client.example.org", "127.0.0.2")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_ACCEPT)

local err = mt.disconnect(conn)
assert(err == nil, err)

-- Test 2: Accept connection from trusted IPv4 address

conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.conninfo(conn, "client.example.org", "123.123.123.123")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_ACCEPT)

local err = mt.disconnect(conn)
assert(err == nil, err)

-- Test 3: Accept connection from trusted IPv6 address

conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.conninfo(conn, "client.example.org", "123::123:123")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_ACCEPT)

local err = mt.disconnect(conn)
assert(err == nil, err)

-- Test 4: Accept message from authenticated sender

conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.macro(conn, SMFIC_CONNECT, "j", "mail.gluet.ch")
assert(err == nil, err)

local err = mt.conninfo(conn, "client.example.org", "7.6.5.4")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.helo(conn, "mail.example.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.macro(conn, SMFIC_MAIL, "{auth_authen}", "from@example.org")
assert(err == nil, err)

local err = mt.mailfrom(conn, "from@example.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_ACCEPT)

local err = mt.disconnect(conn)
assert(err == nil, err)
