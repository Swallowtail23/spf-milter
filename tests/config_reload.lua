conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.macro(conn, SMFIC_CONNECT, "j", "mail.gluet.ch")
assert(err == nil, err)

local err = mt.conninfo(conn, "client.example.org", "123.123.123.123")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.helo(conn, "mail.example.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.mailfrom(conn, "from@example.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.rcptto(conn, "to@gluet.ch")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

SMFIC_DATA = string.byte("T")  -- SMFIC_DATA not exported by miltertest
local err = mt.macro(conn, SMFIC_DATA, "i", "1234567ABC")
assert(err == nil, err)

local err = mt.eom(conn)
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_ACCEPT)

if received_spf then
  assert(mt.eom_check(conn, MT_HDRINSERT, "Received-SPF"))
else
  assert(mt.eom_check(conn, MT_HDRINSERT, "Authentication-Results"))
end

local err = mt.disconnect(conn)
assert(err == nil, err)
