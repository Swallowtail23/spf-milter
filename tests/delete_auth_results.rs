mod common;

pub use common::*;
use spf_milter::*;

#[test]
fn delete_auth_results() {
    let opts = configure_logging(CliOptions::builder())
        .config_file(to_config_file_name(file!()))
        .build();

    let lookup = MockLookup::builder()
        .lookup_txt(|_| Ok(vec!["v=spf1 ~all".into()]))
        .build();

    let miltertest = spawn_miltertest_runner(file!());

    run_with_lookup(opts, lookup).expect("milter execution failed");

    let exit_code = miltertest.join().expect("panic in miltertest runner");
    assert!(exit_code.success(), "miltertest returned error exit code");
}
