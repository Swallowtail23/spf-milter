mod common;

pub use common::*;
use spf_milter::*;

// This test is disabled because it depends on live DNS records.
#[ignore]
#[test]
fn live() {
    let opts = configure_logging(CliOptions::builder())
        .config_file(to_config_file_name(file!()))
        .build();

    let miltertest = spawn_miltertest_runner(file!());

    run(opts).expect("milter execution failed");

    let exit_code = miltertest.join().expect("panic in miltertest runner");
    assert!(exit_code.success(), "miltertest returned error exit code");
}
