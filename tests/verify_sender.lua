--
-- Test 1: Accept authorised sender (HELO)
--

conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.macro(conn, SMFIC_CONNECT, "j", "mail.gluet.ch")
assert(err == nil, err)

local err = mt.conninfo(conn, "client.amy.org", "123.123.123.123")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.helo(conn, "mail.amy.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

-- For completeness, test repeated use of SMTP `HELO` (see debug log):
local err = mt.helo(conn, "mail.amy.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.mailfrom(conn, "from@amy.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.rcptto(conn, "to@gluet.ch")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

SMFIC_DATA = string.byte("T")  -- SMFIC_DATA not exported by miltertest
local err = mt.macro(conn, SMFIC_DATA, "i", "1234567ABC")
assert(err == nil, err)

local err = mt.eom(conn)
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_ACCEPT)

assert(mt.eom_check(conn, MT_HDRINSERT, "Authentication-Results",
  "mail.gluet.ch; spf=pass smtp.helo=mail.amy.org"));

local err = mt.disconnect(conn)
assert(err == nil, err)

--
-- Test 2: Accept authorised sender (MAIL FROM)
--

conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.macro(conn, SMFIC_CONNECT, "j", "mail.gluet.ch")
assert(err == nil, err)

local err = mt.conninfo(conn, "client.amy.org", "123.123.123.123")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

-- Use default (not SPF-enabled) HELO name by not calling `mt.helo`.

local err = mt.mailfrom(conn, "from@amy.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.rcptto(conn, "to@gluet.ch")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

SMFIC_DATA = string.byte("T")  -- SMFIC_DATA not exported by miltertest
local err = mt.macro(conn, SMFIC_DATA, "i", "1234567ABC")
assert(err == nil, err)

local err = mt.eom(conn)
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_ACCEPT)

assert(mt.eom_check(conn, MT_HDRINSERT, "Authentication-Results",
  "mail.gluet.ch; spf=pass smtp.mailfrom=amy.org"));

local err = mt.disconnect(conn)
assert(err == nil, err)

--
-- Test 3: Reject unauthorised sender (HELO)
--

conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.macro(conn, SMFIC_CONNECT, "j", "mail.gluet.ch")
assert(err == nil, err)

local err = mt.conninfo(conn, "client.mallory.org", "123::123:456")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.helo(conn, "mail.mallory.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_REPLYCODE)
-- Cannot check SMTP reply parameters with `miltertest`, see unit tests instead.

local err = mt.disconnect(conn)
assert(err == nil, err)

--
-- Test 4: Reject unauthorised sender (MAIL FROM)
--

conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.macro(conn, SMFIC_CONNECT, "j", "mail.gluet.ch")
assert(err == nil, err)

local err = mt.conninfo(conn, "client.mallory.org", "123::123:456")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

-- Use default (not SPF-enabled) HELO name by not calling `mt.helo`.

local err = mt.mailfrom(conn, "from@mallory.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_REPLYCODE)
-- Cannot check SMTP reply parameters with `miltertest`, see unit tests instead.

local err = mt.disconnect(conn)
assert(err == nil, err)

--
-- Test 5: Multiple messages per connection
--

conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.macro(conn, SMFIC_CONNECT, "j", "mail.gluet.ch")
assert(err == nil, err)

local err = mt.conninfo(conn, "client.amy.org", "123.123.123.123")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

-- Use default (not SPF-enabled) HELO name by not calling `mt.helo`.

-- First message from an authorised sender is accepted.
local err = mt.mailfrom(conn, "from@amy.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.rcptto(conn, "to@gluet.ch")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

SMFIC_DATA = string.byte("T")  -- SMFIC_DATA not exported by miltertest
local err = mt.macro(conn, SMFIC_DATA, "i", "1234567ABC")
assert(err == nil, err)

local err = mt.eom(conn)
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_ACCEPT)

assert(mt.eom_check(conn, MT_HDRINSERT, "Authentication-Results",
  "mail.gluet.ch; spf=pass smtp.mailfrom=amy.org"));

-- Second message from an authorised sender is aborted.
local err = mt.mailfrom(conn, "from@amy.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.rcptto(conn, "to@gluet.ch")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.abort(conn)
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

-- Third message from an unauthorised sender is rejected.
local err = mt.mailfrom(conn, "from@mallory.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_REPLYCODE)
-- Cannot check SMTP reply parameters with `miltertest`, see unit tests instead.

local err = mt.disconnect(conn)
assert(err == nil, err)
