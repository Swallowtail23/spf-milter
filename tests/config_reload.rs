mod common;

pub use common::*;
use spf_milter::*;
use std::{
    ffi::OsString,
    fs,
    net::Ipv4Addr,
    path::{Path, PathBuf},
    process::ExitStatus,
    thread,
    time::Duration,
};
use viaspf::{LookupError, Name};

#[test]
fn config_reload() {
    let opts = configure_logging(CliOptions::builder())
        .config_file(config_file_name())
        .build();

    let lookup = MockLookup::builder()
        .lookup_txt(|name: &Name| match name.as_str() {
            "example.org." => Ok(vec!["v=spf1 a -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .lookup_a(|name: &Name| match name.as_str() {
            "example.org." => Ok(vec![Ipv4Addr::new(123, 123, 123, 123)]),
            _ => Err(LookupError::NoRecords),
        })
        .build();

    let miltertest = spawn_miltertest_runner_with_script(file!(), test_script);

    // For this test the configuration is written to the file system only by the
    // test script, which runs with a delay. Wait a bit before starting the
    // milter.
    thread::sleep(Duration::from_millis(200));

    run_with_lookup(opts, lookup).expect("milter execution failed");

    let exit_code = miltertest.join().expect("panic in miltertest runner");
    assert!(exit_code.success(), "miltertest returned error exit code");
}

fn test_script(file_name: OsString) -> ExitStatus {
    let config_file = config_file_name();

    // Write initial configuration with `Received-SPF` header.
    fs::write(
        &config_file,
        "
        socket = inet:3333@localhost
        verify_helo = no
        header = Received-SPF
        ",
    )
    .unwrap();

    // Now wait as the milter needs to start and read the configuration we’ve
    // just written.
    thread::sleep(Duration::from_millis(200));

    let exit_code = run_miltertest_with_vars(&file_name, vec!["received_spf"]);
    if !exit_code.success() {
        milter::shutdown();
        return exit_code;
    }

    // Write updated configuration with `Authentication-Results` header. Also
    // try changing `socket`, which needs a restart to become effective, but
    // does not prevent reloading.
    fs::write(
        &config_file,
        "
        socket = inet:4444@localhost
        verify_helo = no
        header = Authentication-Results
        ",
    )
    .unwrap();

    reload_config();

    let exit_code = run_miltertest(&file_name);
    if !exit_code.success() {
        milter::shutdown();
        return exit_code;
    }

    // Write configuration with the initial `Received-SPF` header, but since the
    // configuration is now invalid it won’t get reloaded.
    fs::write(
        &config_file,
        "
        socket = inet:4444@localhost
        verify_helo = no
        header = Received-SPF
        invalid_key = invalid_value
        ",
    )
    .unwrap();

    reload_config();

    let exit_code = run_miltertest(&file_name);

    milter::shutdown();

    exit_code
}

fn config_file_name() -> PathBuf {
    // Place configuration file next to the `spf-milter` executable used to run
    // this integration test.
    let file_path = to_config_file_name(file!());
    Path::new(env!("CARGO_BIN_EXE_spf-milter"))
        .parent()
        .unwrap()
        .join(file_path.file_name().unwrap())
}
