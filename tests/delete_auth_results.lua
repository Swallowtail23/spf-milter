conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.macro(conn, SMFIC_CONNECT, "j", "mail.gluet.ch")
assert(err == nil, err)

local err = mt.conninfo(conn, "client.example.org", "123.123.123.123")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.helo(conn, "mail.example.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.mailfrom(conn, "from@example.org")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.rcptto(conn, "to@gluet.ch")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

SMFIC_DATA = string.byte("T")  -- SMFIC_DATA not exported by miltertest
local err = mt.macro(conn, SMFIC_DATA, "i", "1234567ABC")
assert(err == nil, err)

local err = mt.header(conn, "Authentication-Results", [[
(different authserv-id:) myhost.org
  1; spf=pass smtp.mailfrom=example.org]]);
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)
local err = mt.header(conn, "Authentication-Results", [[
(unusual but legal
  comment) "mail.G\luet.ch" (<- attempt to bypass deletion!); spf=pass
  smtp.mailfrom=example.org]]);
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.eom(conn)
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_ACCEPT)

assert(mt.eom_check(conn, MT_HDRDELETE, "Authentication-Results", 2));
assert(mt.eom_check(conn, MT_HDRINSERT, "Authentication-Results",
  "mail.gluet.ch; spf=softfail smtp.mailfrom=example.org"));

local err = mt.disconnect(conn)
assert(err == nil, err)
