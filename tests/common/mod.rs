use spf_milter::{CliOptionsBuilder, LogDestination, LogLevel};
use std::{
    env,
    ffi::{OsStr, OsString},
    iter,
    net::{IpAddr, Ipv4Addr, Ipv6Addr},
    path::PathBuf,
    process::{Command, ExitStatus},
    thread::{self, JoinHandle},
    time::Duration,
};
use viaspf::{Lookup, LookupError, LookupResult, Name};

pub fn configure_logging(opts: CliOptionsBuilder) -> CliOptionsBuilder {
    // Set this environment variable to test debug logging to syslog.
    if env::var("SPF_MILTER_TEST_LOG").is_ok() {
        opts.log_destination(LogDestination::Syslog)
            .log_level(LogLevel::Debug)
    } else {
        opts.log_destination(LogDestination::Stderr)
    }
}

#[derive(Default)]
pub struct MockLookupBuilder {
    lookup_a: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<Ipv4Addr>> + Send + Sync + 'static>>,
    lookup_aaaa: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<Ipv6Addr>> + Send + Sync + 'static>>,
    lookup_mx: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<Name>> + Send + Sync + 'static>>,
    lookup_txt: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<String>> + Send + Sync + 'static>>,
    lookup_ptr: Option<Box<dyn Fn(IpAddr) -> LookupResult<Vec<Name>> + Send + Sync + 'static>>,
}

impl MockLookupBuilder {
    pub fn lookup_a(
        mut self,
        value: impl Fn(&Name) -> LookupResult<Vec<Ipv4Addr>> + Send + Sync + 'static,
    ) -> Self {
        self.lookup_a = Some(Box::new(value));
        self
    }

    pub fn lookup_aaaa(
        mut self,
        value: impl Fn(&Name) -> LookupResult<Vec<Ipv6Addr>> + Send + Sync + 'static,
    ) -> Self {
        self.lookup_aaaa = Some(Box::new(value));
        self
    }

    pub fn lookup_mx(
        mut self,
        value: impl Fn(&Name) -> LookupResult<Vec<Name>> + Send + Sync + 'static,
    ) -> Self {
        self.lookup_mx = Some(Box::new(value));
        self
    }

    pub fn lookup_txt(
        mut self,
        value: impl Fn(&Name) -> LookupResult<Vec<String>> + Send + Sync + 'static,
    ) -> Self {
        self.lookup_txt = Some(Box::new(value));
        self
    }

    pub fn lookup_ptr(
        mut self,
        value: impl Fn(IpAddr) -> LookupResult<Vec<Name>> + Send + Sync + 'static,
    ) -> Self {
        self.lookup_ptr = Some(Box::new(value));
        self
    }

    pub fn build(self) -> MockLookup {
        MockLookup {
            lookup_a: self.lookup_a,
            lookup_aaaa: self.lookup_aaaa,
            lookup_mx: self.lookup_mx,
            lookup_txt: self.lookup_txt,
            lookup_ptr: self.lookup_ptr,
        }
    }
}

#[derive(Default)]
pub struct MockLookup {
    lookup_a: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<Ipv4Addr>> + Send + Sync + 'static>>,
    lookup_aaaa: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<Ipv6Addr>> + Send + Sync + 'static>>,
    lookup_mx: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<Name>> + Send + Sync + 'static>>,
    lookup_txt: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<String>> + Send + Sync + 'static>>,
    lookup_ptr: Option<Box<dyn Fn(IpAddr) -> LookupResult<Vec<Name>> + Send + Sync + 'static>>,
}

impl MockLookup {
    pub fn builder() -> MockLookupBuilder {
        Default::default()
    }
}

impl Lookup for MockLookup {
    fn lookup_a(&self, name: &Name) -> LookupResult<Vec<Ipv4Addr>> {
        self.lookup_a.as_ref().map_or(Err(LookupError::NoRecords), |f| f(name))
    }

    fn lookup_aaaa(&self, name: &Name) -> LookupResult<Vec<Ipv6Addr>> {
        self.lookup_aaaa.as_ref().map_or(Err(LookupError::NoRecords), |f| f(name))
    }

    fn lookup_mx(&self, name: &Name) -> LookupResult<Vec<Name>> {
        self.lookup_mx.as_ref().map_or(Err(LookupError::NoRecords), |f| f(name))
    }

    fn lookup_txt(&self, name: &Name) -> LookupResult<Vec<String>> {
        self.lookup_txt.as_ref().map_or(Err(LookupError::NoRecords), |f| f(name))
    }

    fn lookup_ptr(&self, ip: IpAddr) -> LookupResult<Vec<Name>> {
        self.lookup_ptr.as_ref().map_or(Err(LookupError::NoRecords), |f| f(ip))
    }
}

pub fn to_config_file_name(file_name: &str) -> PathBuf {
    let mut path = PathBuf::from(file_name);
    path.set_extension("conf");
    path
}

/// Spawns a thread that executes the miltertest test for the file
/// `test_file_name`.
pub fn spawn_miltertest_runner(test_file_name: &str) -> JoinHandle<ExitStatus> {
    spawn_miltertest_runner_with_script(test_file_name, |file_name| {
        let exit_code = run_miltertest(file_name);
        milter::shutdown();
        exit_code
    })
}

/// Spawns a thread that executes the given test script closure.
///
/// The test script closure gets passed the file name of the miltertest test for
/// `test_file_name`. The closure should execute this test with `miltertest`,
/// call `milter::shutdown`, and return the test’s exit code.
pub fn spawn_miltertest_runner_with_script<F>(
    test_file_name: &str,
    test_script: F,
) -> JoinHandle<ExitStatus>
where
    F: FnOnce(OsString) -> ExitStatus + Send + Sync + 'static,
{
    // This thread is just for safety, in case the miltertest runner thread
    // below never manages to shut down the milter.
    let _timeout_thread = thread::spawn(|| {
        thread::sleep(Duration::from_secs(20));
        eprintln!("miltertest runner timed out");
        milter::shutdown();
    });

    let file_name = to_miltertest_file_name(test_file_name);

    thread::spawn(move || {
        // Wait just a little while to give the milter time to start up.
        thread::sleep(Duration::from_millis(100));

        test_script(file_name)
    })
}

fn to_miltertest_file_name(file_name: &str) -> OsString {
    let mut path = PathBuf::from(file_name);
    path.set_extension("lua");
    path.into_os_string()
}

const MILTERTEST_PROGRAM: &str = "miltertest";

pub fn run_miltertest(file_name: impl AsRef<OsStr>) -> ExitStatus {
    run_miltertest_with_vars(file_name, iter::empty::<OsString>())
}

pub fn run_miltertest_with_vars<I, S>(file_name: impl AsRef<OsStr>, global_vars: I) -> ExitStatus
where
    I: IntoIterator<Item = S>,
    S: AsRef<OsStr>,
{
    let mut miltertest = Command::new(MILTERTEST_PROGRAM);
    for var in global_vars {
        miltertest.arg("-D").arg(var);
    }
    let output = miltertest
        .arg("-s")
        .arg(file_name)
        .output()
        .expect("miltertest execution failed");

    print_output_stream("STDOUT", output.stdout);
    print_output_stream("STDERR", output.stderr);

    output.status
}

fn print_output_stream(name: &str, output: Vec<u8>) {
    if !output.is_empty() {
        let output = String::from_utf8(output).unwrap();

        eprintln!("{}:", name);

        if output.ends_with('\n') {
            eprint!("{}", &output)
        } else {
            eprintln!("{}", &output)
        }
    }
}
