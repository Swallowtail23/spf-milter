mod common;

pub use common::*;
use spf_milter::*;
use std::net::{Ipv4Addr, Ipv6Addr};
use viaspf::{LookupError, Name};

#[test]
fn verify_sender() {
    let opts = configure_logging(CliOptions::builder())
        .config_file(to_config_file_name(file!()))
        .build();

    let lookup = MockLookup::builder()
        .lookup_txt(|name: &Name| match name.as_str() {
            "amy.org." | "mallory.org." => Ok(vec!["v=spf1 mx -all".into()]),
            "mail.amy.org." | "mail.mallory.org." => Ok(vec!["v=spf1 a -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .lookup_mx(|name: &Name| match name.as_str() {
            "amy.org." => Ok(vec![Name::new("mail.amy.org").unwrap()]),
            "mallory.org." => Ok(vec![Name::new("mail.mallory.org").unwrap()]),
            _ => Err(LookupError::NoRecords),
        })
        .lookup_a(|name: &Name| match name.as_str() {
            "mail.amy.org." => Ok(vec![Ipv4Addr::new(123, 123, 123, 123)]),
            _ => Err(LookupError::NoRecords),
        })
        .lookup_aaaa(|name: &Name| match name.as_str() {
            "mail.mallory.org." => Ok(vec![Ipv6Addr::new(0x123, 0, 0, 0, 0, 0, 0x123, 0x123)]),
            _ => Err(LookupError::NoRecords),
        })
        .build();

    let miltertest = spawn_miltertest_runner(file!());

    run_with_lookup(opts, lookup).expect("milter execution failed");

    let exit_code = miltertest.join().expect("panic in miltertest runner");
    assert!(exit_code.success(), "miltertest returned error exit code");
}
